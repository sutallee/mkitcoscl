#include <new>
#include <stdlib.h>
#include <stdio.h>
#include "synccpp.h"
#include "comment.h"

using namespace ItcCompiler;

SyncCppVisit::SyncCppVisit() throw()
		{
	}

SyncCppVisit::SyncCppVisit(FILE& outputFile) throw():
		TabsVisit(outputFile)
		{
	}

void	SyncCppVisit::preVisit(ParseNodeList& parseNodeList) throw(){
	}

void	SyncCppVisit::preVisit(NamespaceDesc& namespaceDesc) throw(){
	NamespaceNode*	node	= new NamespaceNode(namespaceDesc);
	_namespaceList.put(node);

	appendToHeaderGuard(namespaceDesc.getPrefix().getString());

//	pushTab();
	}

void	SyncCppVisit::preVisit(ItcDesc& itcDesc) throw(){
	fprintf(	&_outputFile,
				"#include \"sync.h\"\n"
				"#include \"oscl/mt/itc/mbox/syncrh.h\"\n\n"
				);

	fprintf(	&_outputFile,
			"using namespace "
			);

	outputNamespaceSeq();

	fprintf(	&_outputFile,
				";\n\n"
				"Sync::Sync(\tOscl::Mt::Itc::PostMsgApi&\tmyPapi,\n"
				"\t\t\tReq::Api&\t\t\t\treqApi\n"
				"\t\t\t) throw():\n"
				"\t\t_sap(reqApi,myPapi)\n"
				"\t\t{\n"
				"\t}\n\n"
				"Req::Api::SAP&\tSync::getSAP() throw(){\n"
				"\treturn _sap;\n"
				"\t}\n\n"
				);
	}

void	SyncCppVisit::preVisit(SyncDesc& syncDesc) throw(){
	_syncDesc	= &syncDesc;
	}

void	SyncCppVisit::preVisit(CancelDesc& cancelDesc) throw(){
	_cancelDesc	= &cancelDesc;
	}

void	SyncCppVisit::preVisit(MessageBodyDesc& messageBodyDesc) throw(){
	}

void	SyncCppVisit::preVisit(MessageDesc& messageDesc) throw(){
	clearMessageFields();
	MessageNode*	messageNode	= new MessageNode(messageDesc);
	_messageNodes.put(messageNode);
	}

void	SyncCppVisit::preVisit(InputDesc& inputDesc) throw(){
	InputNode*	node	= new InputNode(inputDesc);
	_inputNodes.put(node);
	}

void	SyncCppVisit::preVisit(OutputDesc& outputDesc) throw(){
	OutputNode*	node	= new OutputNode(outputDesc);
	_outputNodes.put(node);
	}

void	SyncCppVisit::preVisit(InOutDesc& inOutDesc) throw(){
	InOutNode*	node	= new InOutNode(inOutDesc);
	_inOutNodes.put(node);
	}

void	SyncCppVisit::preVisit(IncludeDesc& includeDesc) throw(){
	IncludeNode*	node	= new IncludeNode(includeDesc);
	_includeNodes.put(node);
	}


void	SyncCppVisit::postVisit(ParseNodeList& parseNodeList) throw(){
	}

void	SyncCppVisit::postVisit(NamespaceDesc& namespaceDesc) throw(){
//	popTab();
	}

void	SyncCppVisit::postVisit(ItcDesc& itcDesc) throw(){
	// Write the server Api 
	outputApi();

	}

void	SyncCppVisit::postVisit(SyncDesc& syncDesc) throw(){
	}

void	SyncCppVisit::postVisit(CancelDesc& cancelDesc) throw(){
	}

void	SyncCppVisit::postVisit(MessageBodyDesc& messageBodyDesc) throw(){
	}

void	SyncCppVisit::postVisit(MessageDesc& messageDesc) throw(){
	if(_syncDesc){
		SyncNode*	syncNode	= new SyncNode(messageDesc,*_syncDesc);
		_syncNodes.put(syncNode);
		syncNode->_inputNodes	= _inputNodes;
		syncNode->_outputNodes	= _outputNodes;
		syncNode->_inOutNodes	= _inOutNodes;
		}
	}

void	SyncCppVisit::postVisit(InputDesc& inputDesc) throw(){
	}

void	SyncCppVisit::postVisit(OutputDesc& outputDesc) throw(){
	}

void	SyncCppVisit::postVisit(InOutDesc& inOutDesc) throw(){
	}

void	SyncCppVisit::postVisit(IncludeDesc& includeDesc) throw(){
	}

void	SyncCppVisit::appendToHeaderGuard(const char* name) throw(){
	_headerGuard	+= "_";
	_headerGuard	+= name;
	}

void	SyncCppVisit::clearMessageFields() throw(){
		InputNode*	inputNode;
		while((inputNode=_inputNodes.get())){
//			delete inputNode;
			}
		OutputNode*	outputNode;
		while((outputNode=_outputNodes.get())){
//			delete outputNode;
			}
		InOutNode*	inOutNode;
		while((inOutNode=_inOutNodes.get())){
			if(!_inOutNodes.first()){
				continue;
				}
//			delete inOutNode;
			}
		_syncDesc	= 0;
		_cancelDesc	= 0;
	}

void	SyncCppVisit::outputApi() throw(){
	for(	SyncNode*	syncNode	= _syncNodes.first();
			syncNode;
			syncNode	= _syncNodes.next(syncNode)
			){
		outputMessageApi(*syncNode);
		}
	}

void	SyncCppVisit::outputMessageApi(	SyncNode&			syncNode
										) throw(){
	fprintf(	&_outputFile,
				"%s\tSync::%s(\t",
				syncNode._syncDesc.getType().getString(),
				syncNode._syncDesc.getName().getString()
				);


	bool	first	= true;
	// Write argument list inputs
	InputNode*	inputNode;

	for(	inputNode=syncNode._inputNodes.first();
			inputNode;
			inputNode=syncNode._inputNodes.next(inputNode)
		){
		if(!first){
			fprintf(&_outputFile,",\n");
			}
		first	= false;
		fprintf(	&_outputFile,
					"\n\t\t\t\t\t\t%s\t%s",
					inputNode->_inputDesc.getType().getString(),
					inputNode->_inputDesc.getName().getString()
					);
		}

	// Write argument list input/outputs
	InOutNode*	inOutNode;
	for(	inOutNode=syncNode._inOutNodes.first();
			inOutNode;
			inOutNode=syncNode._inOutNodes.next(inOutNode)
		){
		if(!first){
			fprintf(&_outputFile,",\n");
			}
		first	= false;
		fprintf(	&_outputFile,
					"\t\t\t\t\t\t%s\t%s",
					inOutNode->_inOutDesc.getType().getString(),
					inOutNode->_inOutDesc.getName().getString()
					);
		}

	// Close argument list
	fprintf(	&_outputFile,
				"\n"
				"\t\t\t\t\t\t) throw(){\n\n"
				);
	if(syncNode._inputNodes.first() || syncNode._inOutNodes.first()){
		fprintf(	&_outputFile,
					"\tOscl::Mt::Itc::SyncReturnHandler\t\tsrh;\n\n"
					"\tReq::Api::%sPayload\t\tpayload(",
					syncNode._messageDesc.getName().getString()
					);

		first	= true;
		// Write argument list inputs

		for(	inputNode=syncNode._inputNodes.first();
				inputNode;
				inputNode=syncNode._inputNodes.next(inputNode)
			){
			if(!first){
				fprintf(&_outputFile,",\n");
				}
			first	= false;
			fprintf(	&_outputFile,
						"\n\t\t\t\t\t\t\t\t\t%s",
						inputNode->_inputDesc.getName().getString()
						);
			}

		// Write argument list input/outputs
		for(	inOutNode=syncNode._inOutNodes.first();
				inOutNode;
			inOutNode=syncNode._inOutNodes.next(inOutNode)
			){
			if(!first){
				fprintf(&_outputFile,",\n");
				}
			first	= false;
			fprintf(	&_outputFile,
						"\t\t\t\t\t\t\t\t\t%s",
						inOutNode->_inOutDesc.getName().getString()
						);
			}

		fprintf(	&_outputFile,
					"\n"
					"\t\t\t\t\t\t\t\t\t);\n\n"
					);

		}
	else {
		fprintf(	&_outputFile,
					"\tOscl::Mt::Itc::SyncReturnHandler\t\tsrh;\n\n"
					"\tReq::Api::%sPayload\t\tpayload;\n\n",
					syncNode._messageDesc.getName().getString()
					);

		}

	fprintf(	&_outputFile,
				"\tReq::Api::%sReq\treq(\t_sap.getReqApi(),\n"
				"\t\t\t\t\t\t\t\t\tpayload,\n"
				"\t\t\t\t\t\t\t\t\tsrh\n"
				"\t\t\t\t\t\t\t\t\t);\n\n"
				"\t_sap.postSync(req);\n",
				syncNode._messageDesc.getName().getString()
				);

	if(syncNode._syncDesc.getField().length()){
		fprintf(	&_outputFile,
				"\n\treturn payload._%s;\n",
				syncNode._syncDesc.getField().getString()
				);
		}

	fprintf(	&_outputFile,
			"\t}\n\n"
			);
	}

void	SyncCppVisit::outputNamespaceSeq() throw(){
	bool	first	= true;
	for(	NamespaceNode*	node=_namespaceList.first();
			node;
			node	= _namespaceList.next(node)
			){
		if(!first){
			fprintf(	&_outputFile,
						"::"
						);
			}
		first	= false;
		fprintf(	&_outputFile,
					"%s",
					node->_namespaceDesc.getPrefix().getString()
					);
		}
	}

