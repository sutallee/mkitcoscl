#include <new>
#include <stdlib.h>
#include <stdio.h>
#include "respmemcpp.h"
#include "comment.h"

using namespace ItcCompiler;

RespMemCppVisit::RespMemCppVisit() throw():
		_cancelDesc(0)
		{
	}

RespMemCppVisit::RespMemCppVisit(FILE& outputFile) throw():
		TabsVisit(outputFile),
		_cancelDesc(0)
		{
	}

void	RespMemCppVisit::preVisit(ParseNodeList& parseNodeList) throw(){
	}

void	RespMemCppVisit::preVisit(NamespaceDesc& namespaceDesc) throw(){
	NamespaceNode*	node	= new NamespaceNode(namespaceDesc);
	_namespaceList.put(node);
	}

void	RespMemCppVisit::preVisit(ItcDesc& itcDesc) throw(){
	fprintf(	&_outputFile,
				"#include <new>\n"
				"#include \"respmem.h\"\n\n"
				);

	if(_namespaceList.first()){
		fprintf(	&_outputFile,
					"using namespace "
					);
		outputNamespaceSeq();
		fprintf(	&_outputFile,
					";\n\n"
					);
		}
	}

void	RespMemCppVisit::preVisit(SyncDesc& syncDesc) throw(){
	_syncDesc	= &syncDesc;
	}

void	RespMemCppVisit::preVisit(CancelDesc& cancelDesc) throw(){
	_cancelDesc	= &cancelDesc;
	}

void	RespMemCppVisit::preVisit(MessageBodyDesc& messageBodyDesc) throw(){
	}

void	RespMemCppVisit::preVisit(MessageDesc& messageDesc) throw(){
	clearMessageFields();
	MessageNode*	messageNode	= new MessageNode(messageDesc);
	_messageNodes.put(messageNode);
	}

void	RespMemCppVisit::preVisit(InputDesc& inputDesc) throw(){
	InputNode*	node	= new InputNode(inputDesc);
	_inputNodes.put(node);
	}

void	RespMemCppVisit::preVisit(OutputDesc& outputDesc) throw(){
	OutputNode*	node	= new OutputNode(outputDesc);
	_outputNodes.put(node);
	}

void	RespMemCppVisit::preVisit(InOutDesc& inOutDesc) throw(){
	InOutNode*	node	= new InOutNode(inOutDesc);
	_inOutNodes.put(node);
	}

void	RespMemCppVisit::preVisit(IncludeDesc& includeDesc) throw(){
	IncludeNode*	node	= new IncludeNode(includeDesc);
	_includeNodes.put(node);
	}


void	RespMemCppVisit::postVisit(ParseNodeList& parseNodeList) throw(){
	}

void	RespMemCppVisit::postVisit(NamespaceDesc& namespaceDesc) throw(){
	}

void	RespMemCppVisit::postVisit(ItcDesc& itcDesc) throw(){
	}

void	RespMemCppVisit::postVisit(SyncDesc& syncDesc) throw(){
	}

void	RespMemCppVisit::postVisit(CancelDesc& cancelDesc) throw(){
	_cancelDesc	= &cancelDesc;
	}

void	RespMemCppVisit::postVisit(MessageBodyDesc& messageBodyDesc) throw(){
	}

void	RespMemCppVisit::postVisit(MessageDesc& messageDesc) throw(){
	outputConstructor(messageDesc);
	if(_cancelDesc){
		outputCancelConstructor(messageDesc);
		}
	_cancelDesc	= 0;
	}

void	RespMemCppVisit::postVisit(InputDesc& inputDesc) throw(){
	}

void	RespMemCppVisit::postVisit(OutputDesc& outputDesc) throw(){
	}

void	RespMemCppVisit::postVisit(InOutDesc& inOutDesc) throw(){
	}

void	RespMemCppVisit::postVisit(IncludeDesc& includeDesc) throw(){
	}

void	RespMemCppVisit::clearMessageFields() throw(){
		InputNode*	inputNode;
		while((inputNode=_inputNodes.get())){
//			delete inputNode;
			}
		OutputNode*	outputNode;
		while((outputNode=_outputNodes.get())){
//			delete outputNode;
			}
		InOutNode*	inOutNode;
		while((inOutNode=_inOutNodes.get())){
			if(!_inOutNodes.first()){
				continue;
				}
//			delete inOutNode;
			}
		_syncDesc	= 0;
		_cancelDesc	= 0;
	}

void	RespMemCppVisit::outputConstructor(MessageDesc& messageDesc) throw(){
	fprintf(	&_outputFile,
				"Resp::Api::%sResp&\n"
				"\tResp::%sMem::build(\tReq::Api::SAP&\t\t\tsap,\n"
				"\t\t\t\t\t\t\tResp::Api&\t\t\t\trespApi,\n"
				"\t\t\t\t\t\t\tOscl::Mt::Itc::PostMsgApi&\tclientPapi",
				messageDesc.getName().getString(),
				messageDesc.getName().getString()
				);

	bool	first = false;
	// Write Constructor Inputs
	InputNode*	inputNode;
	for(	inputNode=_inputNodes.first();
			inputNode;
			inputNode=_inputNodes.next(inputNode)
		){
		if(!first){
			fprintf(&_outputFile,",\n");
			}
		first	= false;
		fprintf(	&_outputFile,
					"\t\t\t\t\t\t\t%s\t%s",
					inputNode->_inputDesc.getType().getString(),
					inputNode->_inputDesc.getName().getString()
					);
		}

	// Write Constructor Inputs/Outputs
	InOutNode*	inOutNode;
	for(	inOutNode=_inOutNodes.first();
			inOutNode;
			inOutNode=_inOutNodes.next(inOutNode)
		){
		if(!first){
			fprintf(&_outputFile,",\n");
			}
		first	= false;
		fprintf(	&_outputFile,
					"\t\t\t\t\t\t\t%s\t%s",
					inOutNode->_inOutDesc.getType().getString(),
					inOutNode->_inOutDesc.getName().getString()
					);
		}

	fprintf(	&_outputFile,
				"\n\t\t\t\t\t\t\t) throw(){\n"
				"\tReq::Api::%sPayload*\n"
				"\t\tp\t=\tnew(&payload)\n"
				"\t\t\t\tReq::Api::%sPayload(\n",
				messageDesc.getName().getString(),
				messageDesc.getName().getString()
				);

	first = true;
	// Write Constructor Inputs
	for(	inputNode=_inputNodes.first();
			inputNode;
			inputNode=_inputNodes.next(inputNode)
		){
		if(!first){
			fprintf(&_outputFile,",\n");
			}
		first	= false;
		fprintf(	&_outputFile,
					"\t\t\t\t\t\t\t%s",
					inputNode->_inputDesc.getName().getString()
					);
		}

	// Write Constructor Inputs/Outputs
	for(	inOutNode=_inOutNodes.first();
			inOutNode;
			inOutNode=_inOutNodes.next(inOutNode)
		){
		if(!first){
			fprintf(&_outputFile,",\n");
			}
		first	= false;
		fprintf(	&_outputFile,
					"\t\t\t\t\t\t\t%s",
					inOutNode->_inOutDesc.getName().getString()
					);
		}

	if(first){
		fprintf(	&_outputFile,
					");\n"
					);
		}
	else {
		fprintf(	&_outputFile,
					"\n\t\t\t\t\t\t\t);\n"
					);
		}

	fprintf(	&_outputFile,
				"\tResp::Api::%sResp*\n"
				"\t\tr\t=\tnew(&resp)\n"
				"\t\t\tResp::Api::%sResp(\tsap.getReqApi(),\n"
				"\t\t\t\t\t\t\t\t\trespApi,\n"
				"\t\t\t\t\t\t\t\t\tclientPapi,\n"
				"\t\t\t\t\t\t\t\t\t*p\n"
				"\t\t\t\t\t\t\t\t\t);\n\n"
				"\treturn *r;\n"
				"\t}\n\n",
				messageDesc.getName().getString(),
				messageDesc.getName().getString()
				);
	}

void	RespMemCppVisit::outputNamespaceSeq() throw(){
	bool	first	= true;
	for(	NamespaceNode*	node=_namespaceList.first();
			node;
			node	= _namespaceList.next(node)
			){
		if(!first){
			fprintf(	&_outputFile,
						"::"
						);
			}
		first	= false;
		fprintf(	&_outputFile,
					"%s",
					node->_namespaceDesc.getPrefix().getString()
					);
		}
	}

void	RespMemCppVisit::outputCancelConstructor(MessageDesc& messageDesc) throw(){
	fprintf(	&_outputFile,
				"Resp::Api::Cancel%sResp&\n"
				"\tResp::Cancel%sMem::build(\tReq::Api::SAP&\tsap,\n"
				"\t\t\t\t\t\t\tResp::Api&\t\t\t\trespApi,\n"
				"\t\t\t\t\t\t\tOscl::Mt::Itc::PostMsgApi&\tclientPapi,\n"
				"\t\t\t\t\t\tReq::Api::%sReq&\trequestToCancel\n"
				"\t\t\t\t\t\t) throw(){\n\n",
				messageDesc.getName().getString(),
				messageDesc.getName().getString(),
				messageDesc.getName().getString()
				);

	fprintf(	&_outputFile,
				"\tReq::Api::Cancel%sPayload*\n"
				"\t\tp\t=\tnew(&payload)\n"
				"\t\t\t\tReq::Api::Cancel%sPayload(\trequestToCancel);\n\n",
				messageDesc.getName().getString(),
				messageDesc.getName().getString()
				);

	fprintf(	&_outputFile,
				"\tResp::Api::Cancel%sResp*\n"
				"\t\tr\t=\tnew(&resp)\n"
				"\t\t\tResp::Api::Cancel%sResp(\tsap.getReqApi(),\n"
				"\t\t\t\t\t\t\trespApi,\n"
				"\t\t\t\t\t\t\tclientPapi,\n"
				"\t\t\t\t\t\t\t*p\n"
				"\t\t\t\t\t\t\t);\n\n"
				"\treturn *r;\n"
				"\t}\n\n",
				messageDesc.getName().getString(),
				messageDesc.getName().getString()
				);
	}
