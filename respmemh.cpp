#include <new>
#include <stdlib.h>
#include <stdio.h>
#include "respmemh.h"
#include "comment.h"

using namespace ItcCompiler;

RespMemHeaderVisit::RespMemHeaderVisit() throw():
		_cancelDesc(0)
		{
	}

RespMemHeaderVisit::RespMemHeaderVisit(FILE& outputFile) throw():
		TabsVisit(outputFile),
		_cancelDesc(0)
		{
	}

void	RespMemHeaderVisit::preVisit(ParseNodeList& parseNodeList) throw(){
	}

void	RespMemHeaderVisit::preVisit(NamespaceDesc& namespaceDesc) throw(){
	NamespaceNode*	node	= new NamespaceNode(namespaceDesc);
	_namespaceList.put(node);

	appendToHeaderGuard(namespaceDesc.getPrefix().getString());

//	pushTab();
	}

void	RespMemHeaderVisit::preVisit(ItcDesc& itcDesc) throw(){
	_headerGuard += "_itc_respmemh_";
	_headerGuard.convertToLowerCase();

	fprintf(	&_outputFile,
				"#ifndef %s\n"
				"#define %s\n\n",
				_headerGuard.getString(),
				_headerGuard.getString()
				);

	fprintf(	&_outputFile,
				"#include \"respapi.h\"\n"
				"#include \"oscl/memory/block.h\"\n"
				"\n"
				);

	const char	itcGeneralComment[] = {
		"These record types in this file describe blocks of memory that are\n"
		"large enough and appropriately aligned such that placement new operations\n"
		"creating instances of their corresponding objects can safely be performed.\n"
		"These records are useful to most clients that use the server's ITC\n"
		"interface asynchronously.\n"
		};
	printComment(	&_outputFile,	// outputFile
					4,				// tabWidthInCharacters
					60,				// paragraphWidthInCharacters
					0,				// nTabIndention
					itcGeneralComment
					);

	fprintf(	&_outputFile,
				"\n"
				);

	for(	NamespaceNode*	node=_namespaceList.first();
			node;
			node	= _namespaceList.next(node)
			){
		fprintf(	&_outputFile,
					"/** */\n"
					);
		fprintf(	&_outputFile,
					"namespace %s {\n\n",
					node->_namespaceDesc.getPrefix().getString()
					);
		}
	fprintf(	&_outputFile,
				"/** */\n"
				"namespace Resp {\n\n"
				);

	}

void	RespMemHeaderVisit::preVisit(SyncDesc& syncDesc) throw(){
	_syncDesc	= &syncDesc;
	}

void	RespMemHeaderVisit::preVisit(CancelDesc& cancelDesc) throw(){
	_cancelDesc	= &cancelDesc;
	}

void	RespMemHeaderVisit::preVisit(MessageBodyDesc& messageBodyDesc) throw(){
	}

void	RespMemHeaderVisit::preVisit(MessageDesc& messageDesc) throw(){
	clearMessageFields();
	MessageNode*	messageNode	= new MessageNode(messageDesc);
	_messageNodes.put(messageNode);
	}

void	RespMemHeaderVisit::preVisit(InputDesc& inputDesc) throw(){
	InputNode*	node	= new InputNode(inputDesc);
	_inputNodes.put(node);
	}

void	RespMemHeaderVisit::preVisit(OutputDesc& outputDesc) throw(){
	OutputNode*	node	= new OutputNode(outputDesc);
	_outputNodes.put(node);
	}

void	RespMemHeaderVisit::preVisit(InOutDesc& inOutDesc) throw(){
	InOutNode*	node	= new InOutNode(inOutDesc);
	_inOutNodes.put(node);
	}

void	RespMemHeaderVisit::preVisit(IncludeDesc& includeDesc) throw(){
	IncludeNode*	node	= new IncludeNode(includeDesc);
	_includeNodes.put(node);
	}


void	RespMemHeaderVisit::postVisit(ParseNodeList& parseNodeList) throw(){
	}

void	RespMemHeaderVisit::postVisit(NamespaceDesc& namespaceDesc) throw(){
//	popTab();
	}

void	RespMemHeaderVisit::postVisit(ItcDesc& itcDesc) throw(){
	// Close up the namespaces
	fprintf(	&_outputFile,
				"}\n"
				);
	for(	NamespaceNode*	node=_namespaceList.first();
			node;
			node	= _namespaceList.next(node)
			){
		fprintf(	&_outputFile,
					"}\n"
					);
		}
	// Close up the header guard
	fprintf(	&_outputFile,
				"#endif\n"
				);

	}

void	RespMemHeaderVisit::postVisit(SyncDesc& syncDesc) throw(){
	}

void	RespMemHeaderVisit::postVisit(CancelDesc& cancelDesc) throw(){
	}

void	RespMemHeaderVisit::postVisit(MessageBodyDesc& messageBodyDesc) throw(){
	}

void	RespMemHeaderVisit::postVisit(MessageDesc& messageDesc) throw(){
	outputRespMessage(messageDesc);
	if(_cancelDesc){
		outputCancelRespMessage(messageDesc);
		_cancelDesc	= 0;
		}
	}

void	RespMemHeaderVisit::postVisit(InputDesc& inputDesc) throw(){
	}

void	RespMemHeaderVisit::postVisit(OutputDesc& outputDesc) throw(){
	}

void	RespMemHeaderVisit::postVisit(InOutDesc& inOutDesc) throw(){
	}

void	RespMemHeaderVisit::postVisit(IncludeDesc& includeDesc) throw(){
	}

void	RespMemHeaderVisit::appendToHeaderGuard(const char* name) throw(){
	_headerGuard	+= "_";
	_headerGuard	+= name;
	}

void	RespMemHeaderVisit::clearMessageFields() throw(){
		InputNode*	inputNode;
		while((inputNode=_inputNodes.get())){
//			delete inputNode;
			}
		OutputNode*	outputNode;
		while((outputNode=_outputNodes.get())){
//			delete outputNode;
			}
		InOutNode*	inOutNode;
		while((inOutNode=_inOutNodes.get())){
			if(!_inOutNodes.first()){
				continue;
				}
//			delete inOutNode;
			}
		_syncDesc	= 0;
		_cancelDesc	= 0;
	}

void	RespMemHeaderVisit::outputRespMessage(MessageDesc& messageDesc) throw(){
	fprintf(	&_outputFile,
				"/**\tThis record describes a block of memory that is\n"
				"\tlarge enough (and properly aligned) to hold a\n"
				"\t%sResp and its corresponding Req::Api::%sPayload.\n"
				"\tClients which use the corresponding server asynchronously\n"
				"\tfrequently need to reservere/manage memory for for these\n"
				"\tITC interactions and instances of this record are useful\n"
				"\tfor that purpose.\n"
				" */\n"
				"struct %sMem {\n"
				"\t/**\tThis is memory enough for the response message.\n"
				"\t */\n"
				"\tOscl::Memory::AlignedBlock<sizeof(Resp::Api::%sResp)>\tresp;\n\n"
				"\t/**\tThis is memory enough for the message payload.\n"
				"\t */\n"
				"\tOscl::Memory::AlignedBlock<sizeof(Req::Api::%sPayload)>\tpayload;\n\n"
				,
				messageDesc.getName().getString(),
				messageDesc.getName().getString(),
				messageDesc.getName().getString(),
				messageDesc.getName().getString(),
				messageDesc.getName().getString()
				);
	// Open Constructor
	fprintf(	&_outputFile,
				"\t/**\tThis operation creates a response message using this\n"
				"\t\t block of memory.\n"
				"\t */\n"
				"\t"
				);
	outputNamespaceSeq();

	fprintf(	&_outputFile,
				"Resp::Api::%sResp&\n"
				"\t\tbuild(\t",
				messageDesc.getName().getString()
				);

	outputNamespaceSeq();

	fprintf(	&_outputFile,
				"Req::Api::SAP&\tsap,\n"
				"\t\t\t\t"
				);

	outputNamespaceSeq();

	fprintf(	&_outputFile,
				"Resp::Api&\t\trespApi,\n"
				"\t\t\t\tOscl::Mt::Itc::PostMsgApi&\t\tclientPapi"
				);

	bool	first	= false;
	// Write Constructor Inputs
	InputNode*	inputNode;
	for(	inputNode=_inputNodes.first();
			inputNode;
			inputNode=_inputNodes.next(inputNode)
		){
		if(!first){
			fprintf(&_outputFile,",\n");
			}
		first	= false;
		fprintf(	&_outputFile,
					"\t\t\t\t%s\t%s",
					inputNode->_inputDesc.getType().getString(),
					inputNode->_inputDesc.getName().getString()
					);
		}

	// Write Constructor Inputs/Outputs
	InOutNode*	inOutNode;
	for(	inOutNode=_inOutNodes.first();
			inOutNode;
			inOutNode=_inOutNodes.next(inOutNode)
		){
		if(!first){
			fprintf(&_outputFile,",\n");
			}
		first	= false;
		fprintf(	&_outputFile,
					"\t\t\t\t%s\t%s",
					inOutNode->_inOutDesc.getType().getString(),
					inOutNode->_inOutDesc.getName().getString()
					);
		}

	// Close Constructor
	if(!first){
		fprintf(&_outputFile,"\n");
		}

	fprintf(	&_outputFile,
				"\t\t\t\t) throw();\n\n"
				"\t};\n\n"
				);
	}

void	RespMemHeaderVisit::outputApi() throw(){
	for(	MessageNode*	messageNode	= _messageNodes.first();
			messageNode;
			messageNode	= _messageNodes.next(messageNode)
			){
		outputMessageApi(messageNode->_messageDesc);
		}
	}

void	RespMemHeaderVisit::outputMessageApi(const MessageDesc& messageDesc) throw(){
	fprintf(	&_outputFile,
				"\t\t/** This operation is invoked within the client thread\n"
				"\t\t\twhenever a %sResp message is received.\n"
				"\t\t */\n"
				"\t\tvirtual void\tresponse(\t",
				messageDesc.getName().getString()
				);

	outputNamespaceSeq();

	fprintf(	&_outputFile,
				"\n\t\t\t\t\t\t\t\t\tResp::Api::%sResp& msg\n"
				"\t\t\t\t\t\t\t\t\t) throw()=0;\n\n",
				messageDesc.getName().getString()
				);
	}

void	RespMemHeaderVisit::outputNamespaceSeq() throw(){
	for(	NamespaceNode*	node=_namespaceList.first();
			node;
			node	= _namespaceList.next(node)
			){
		fprintf(	&_outputFile,
					"%s::",
					node->_namespaceDesc.getPrefix().getString()
					);
		}
	}

void	RespMemHeaderVisit::outputCancelRespMessage(MessageDesc& messageDesc) throw(){
	fprintf(	&_outputFile,
				"/**\tThis record describes a block of memory that is\n"
				"\tlarge enough (and properly aligned) to hold a\n"
				"\tCancel%sResp and its corresponding\n"
				"\tReq::Api::Cancel%sPayload. Clients, which use the\n"
				"\tcorresponding server asynchronously, frequently need\n"
				"\tto reservere/manage memory for for these ITC interactions\n"
				"\tand instances of this record are useful for that purpose.\n"
				" */\n"
				"struct Cancel%sMem {\n"
				"\t/**\tThis is memory enough for the response message.\n"
				"\t */\n"
				"\tOscl::Memory::AlignedBlock<sizeof(Resp::Api::Cancel%sResp)>\tresp;\n\n"
				"\t/**\tThis is memory enough for the message payload.\n"
				"\t */\n"
				"\tOscl::Memory::AlignedBlock<sizeof(Req::Api::Cancel%sPayload)>\tpayload;\n\n"
				,
				messageDesc.getName().getString(),
				messageDesc.getName().getString(),
				messageDesc.getName().getString(),
				messageDesc.getName().getString(),
				messageDesc.getName().getString()
				);
	// Open Constructor
	fprintf(	&_outputFile,
				"\t/**\tThis operation creates a response message using this\n"
				"\t\t block of memory.\n"
				"\t */\n"
				"\t"
				);
	outputNamespaceSeq();

	fprintf(	&_outputFile,
				"Resp::Api::Cancel%sResp&\n"
				"\t\tbuild(\t",
				messageDesc.getName().getString()
				);

	outputNamespaceSeq();

	fprintf(	&_outputFile,
				"Req::Api::SAP&\tsap,\n"
				"\t\t\t\t"
				);

	outputNamespaceSeq();

	fprintf(	&_outputFile,
				"Resp::Api&\t\trespApi,\n"
				"\t\t\t\tOscl::Mt::Itc::PostMsgApi&\t\tclientPapi,\n"
				"\t\t\t\tReq::Api::%sReq&\trequestToCancel\n"
				"\t\t\t\t) throw();\n\n",
				messageDesc.getName().getString()
				);

	fprintf(	&_outputFile,
				"\t};\n\n"
				);
	}

