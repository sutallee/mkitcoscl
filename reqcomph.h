#ifndef _reqcomphh_
#define _reqcomphh_
#include "tabs.h"
#include "nodes.h"

/** */
namespace ItcCompiler {

/** */
class ReqCompHeaderVisit : public TabsVisit {
	public:
		/** */
		Oscl::Strings::Dynamic			_headerGuard;
		/** */
		Oscl::Queue<NamespaceNode>		_namespaceList;
		/** */
		Oscl::Queue<IncludeNode>		_includeNodes;
		/** */
		Oscl::Queue<MessageNode>		_messageNodes;

	private:
		/** */
		Oscl::Queue<InputNode>			_inputNodes;
		/** */
		Oscl::Queue<OutputNode>			_outputNodes;
		/** */
		Oscl::Queue<InOutNode>			_inOutNodes;
		/** */
		SyncDesc*						_syncDesc;
		/** */
		CancelDesc*						_cancelDesc;
		/** */
		Oscl::Queue<CancelNode>			_cancelNodes;

	public:
		/** */
		ReqCompHeaderVisit() throw();
		/** */
		ReqCompHeaderVisit(FILE& outputFile) throw();
		/** */
		void	preVisit(ParseNodeList& parseNodeList) throw();
		/** */
		void	preVisit(NamespaceDesc& namespaceDesc) throw();
		/** */
		void	preVisit(ItcDesc& itcDesc) throw();
		/** */
		void	preVisit(SyncDesc& syncDesc) throw();
		/** */
		void	preVisit(CancelDesc& cancelDesc) throw();
		/** */
		void	preVisit(MessageBodyDesc& messageBodyDesc) throw();
		/** */
		void	preVisit(MessageDesc& messageDesc) throw();
		/** */
		void	preVisit(InputDesc& inputDesc) throw();
		/** */
		void	preVisit(OutputDesc& outputDesc) throw();
		/** */
		void	preVisit(InOutDesc& inOutDesc) throw();
		/** */
		void	preVisit(IncludeDesc& includeDesc) throw();

	public:
		/** */
		void	postVisit(ParseNodeList& parseNodeList) throw();
		/** */
		void	postVisit(NamespaceDesc& namespaceDesc) throw();
		/** */
		void	postVisit(ItcDesc& itcDesc) throw();
		/** */
		void	postVisit(SyncDesc& syncDesc) throw();
		/** */
		void	postVisit(CancelDesc& cancelDesc) throw();
		/** */
		void	postVisit(MessageBodyDesc& messageBodyDesc) throw();
		/** */
		void	postVisit(MessageDesc& messageDesc) throw();
		/** */
		void	postVisit(InputDesc& inputDesc) throw();
		/** */
		void	postVisit(OutputDesc& outputDesc) throw();
		/** */
		void	postVisit(InOutDesc& inOutDesc) throw();
		/** */
		void	postVisit(IncludeDesc& includeDesc) throw();
	private:
		/** */
		void	appendToHeaderGuard(const char* name) throw();
		/** */
		void	clearMessageFields() throw();
		/** */
		void	outputNamespaceSeq() throw();

	private:
		/** */
		void	outputMemberFunctionVariableDeclarations() throw();
		/** */
		void	outputConstructorDeclaration() throw();
		/** */
		void	outputMemberFunctionDeclarations() throw();
		/** */
		void	outputMemberFunctionDeclaration(MessageNode& messageNode) throw();
		/** */
		void	outputConstructorImplementation() throw();
		/** */
		void	outputInitializerList(MessageNode& messageNode) throw();
		/** */
		void	outputMemberFunctionImplementations() throw();
		/** */
		void	outputMemberFunctionImplementation(MessageNode& messageNode) throw();
		/** */
		void	outputMemberFunctionVariableDeclaration(MessageNode& messageNode) throw();
		/** */
		void	outputMessageMemberFunctionDeclaration(MessageNode& messageNode) throw();
		/** */
		void	outputMemberFunctionArgument(MessageNode& messageNode,const char* leadingTabs) throw();
		/** */
		void	outputArgumentList(MessageNode& messageNode,const char* leadingTabs) throw();
		/** */
		void	outputCallArgumentList(MessageNode& messageNode,const char* leadingTabs) throw();
		/** */
		void	outputCancelInitializerList(CancelNode& cancelNode) throw();
		/** */
		void	outputCancelMemberFunctionImplementation(CancelNode& cancelNode) throw();
		/** */
		void	outputCancelMemberFunctionVariableDeclaration(CancelNode& cancelNode) throw();
		/** */
		void	outputCancelMemberFunctionDeclaration(CancelNode& cancelNode) throw();
		/** */
		void	outputCancelMemberFunctionArgument(CancelNode& cancelNode,const char* leadingTabs) throw();
		/** */
		void	outputCancelArgumentList(CancelNode& cancelNode,const char* leadingTabs) throw();
		/** */
		void	outputCancelCallArgumentList(CancelNode& cancelNode,const char* leadingTabs) throw();
	};

}

#endif
