#include <new>
#include <stdlib.h>
#include <stdio.h>
#include "reqcomph.h"
#include "comment.h"

using namespace ItcCompiler;

ReqCompHeaderVisit::ReqCompHeaderVisit() throw()
		{
	}

ReqCompHeaderVisit::ReqCompHeaderVisit(FILE& outputFile) throw():
		TabsVisit(outputFile)
		{
	}

void	ReqCompHeaderVisit::preVisit(ParseNodeList& parseNodeList) throw(){
	}

void	ReqCompHeaderVisit::preVisit(NamespaceDesc& namespaceDesc) throw(){
	NamespaceNode*	node	= new NamespaceNode(namespaceDesc);
	_namespaceList.put(node);

	appendToHeaderGuard(namespaceDesc.getPrefix().getString());
	}

void	ReqCompHeaderVisit::preVisit(ItcDesc& itcDesc) throw(){
	_headerGuard += "_reqcomph_";
	_headerGuard.convertToLowerCase();

	fprintf(	&_outputFile,
				"#ifndef %s\n"
				"#define %s\n\n",
				_headerGuard.getString(),
				_headerGuard.getString()
				);

	fprintf(	&_outputFile,
				"#include \"reqapi.h\"\n\n"
				);

	for(	NamespaceNode*	node=_namespaceList.first();
			node;
			node	= _namespaceList.next(node)
			){
		fprintf(	&_outputFile,
					"/** */\n"
					);
		fprintf(	&_outputFile,
					"namespace %s {\n\n",
					node->_namespaceDesc.getPrefix().getString()
					);
		}
	fprintf(	&_outputFile,
				"/** */\n"
				"namespace Req {\n\n"
				);

	const char	itcGeneralComment[] = {
		"This template class implements a kind of GOF decorator\n"
		"pattern allows the context to employ composition instead\n"
		"of inheritance. Frequently, a context may need to implement\n"
		"more than one interface that may result in method name clashes.\n"
		"This template solves the problem by implementing\n"
		"the interface and then invoking member function\n"
		"pointers in the context instead.\n"
		"One instance of this object is created in the context\n"
		"for each interface of this type used in the context.\n"
		"Each instance is constructed such that it refers to different\n"
		"member functions within the context.\n"
		"The interface of this object is passed to any\n"
		"object that requires the interface. When the\n"
		"object invokes any of the operations of the API,\n"
		"the corresponding member function of the context\n"
		"will subsequently be invoked.\n"
		};
	printComment(	&_outputFile,	// outputFile
					4,				// tabWidthInCharacters
					60,				// paragraphWidthInCharacters
					0,				// nTabIndention
					itcGeneralComment
					);

	printComment(	&_outputFile,	// outputFile
					4,				// tabWidthInCharacters
					60,				// paragraphWidthInCharacters
					0,				// nTabIndention
					itcDesc.getComment().getString()
					);
	fprintf(	&_outputFile,
				"template <class Context>\n"
				"class Composer : public Api {\n"
				);
	}

void	ReqCompHeaderVisit::preVisit(SyncDesc& syncDesc) throw(){
	_syncDesc	= &syncDesc;
	}

void	ReqCompHeaderVisit::preVisit(CancelDesc& cancelDesc) throw(){
	_cancelDesc	= &cancelDesc;
	}

void	ReqCompHeaderVisit::preVisit(MessageBodyDesc& messageBodyDesc) throw(){
	}

void	ReqCompHeaderVisit::preVisit(MessageDesc& messageDesc) throw(){
	clearMessageFields();
	MessageNode*	messageNode	= new MessageNode(messageDesc);
	_messageNodes.put(messageNode);
	}

void	ReqCompHeaderVisit::preVisit(InputDesc& inputDesc) throw(){
	InputNode*	node	= new InputNode(inputDesc);
	_inputNodes.put(node);
	}

void	ReqCompHeaderVisit::preVisit(OutputDesc& outputDesc) throw(){
	OutputNode*	node	= new OutputNode(outputDesc);
	_outputNodes.put(node);
	}

void	ReqCompHeaderVisit::preVisit(InOutDesc& inOutDesc) throw(){
	InOutNode*	node	= new InOutNode(inOutDesc);
	_inOutNodes.put(node);
	}

void	ReqCompHeaderVisit::preVisit(IncludeDesc& includeDesc) throw(){
	IncludeNode*	node	= new IncludeNode(includeDesc);
	_includeNodes.put(node);
	}


void	ReqCompHeaderVisit::postVisit(ParseNodeList& parseNodeList) throw(){
	}

void	ReqCompHeaderVisit::postVisit(NamespaceDesc& namespaceDesc) throw(){
//	popTab();
	}

void	ReqCompHeaderVisit::postVisit(ItcDesc& itcDesc) throw(){

	// At this point, we have visited all of the nodes within
	// ITC description the parse tree, and have therefore
	// collected all of the information for all of the messages
	// into local data stores.
	// We are now ready to generate all of the remaining
	// output for the class using those data stores.
	// The opening namespace and template class stuff has
	// been written. Now we need to:
	// 1) Write the type typedefs for the member function prototypes.
	//    However, given that is mostly a convenience for the human
	//    developer, we may skip that and just generate the signatures
	//    individually. This would also reduce the number of types
	//    maintained by the compiler.
	// 2) Declare the Context reference member variable.
	// 3) Declare each member function pointer variable.
	// 4) Declare the constructor.
	// 5) Declare each member function.
	// 6) Close the class declaration.
	// 7) Write the constructor implementation.
	// 8) Write each member function implementation.
	// 9) Close up the namespaces.
	// 10) Close up the header guard.

	// Declare the Context reference member variable
	fprintf(	&_outputFile,
				"\tprivate:\n"
				"\t\t/** A reference to the Context.\n"
				"\t\t */\n"
				"\t\tContext&\t_context;\n\n"
				);

	outputMemberFunctionVariableDeclarations();

	outputConstructorDeclaration();

	outputMemberFunctionDeclarations();

	// Close up the "class Api"
	fprintf(	&_outputFile,
				"\t};\n\n"
				);

	outputConstructorImplementation();

	outputMemberFunctionImplementations();

	// Close up the namespaces
	for(	NamespaceNode*	node=_namespaceList.first();
			node;
			node	= _namespaceList.next(node)
			){
		fprintf(	&_outputFile,
					"}\n"
					);
		}

	// Close up the header guard
	fprintf(	&_outputFile,
				"}\n"
				"#endif\n"
				);
	}

void	ReqCompHeaderVisit::postVisit(SyncDesc& syncDesc) throw(){
	}

void	ReqCompHeaderVisit::postVisit(CancelDesc& cancelDesc) throw(){
	}

void	ReqCompHeaderVisit::postVisit(MessageBodyDesc& messageBodyDesc) throw(){
	}

void	ReqCompHeaderVisit::postVisit(MessageDesc& messageDesc) throw(){
	if(_cancelDesc){
		CancelNode*	cancelNode	= new CancelNode(messageDesc);
		_cancelNodes.put(cancelNode);
		}
	}

void	ReqCompHeaderVisit::postVisit(InputDesc& inputDesc) throw(){
	}

void	ReqCompHeaderVisit::postVisit(OutputDesc& outputDesc) throw(){
	}

void	ReqCompHeaderVisit::postVisit(InOutDesc& inOutDesc) throw(){
	}

void	ReqCompHeaderVisit::postVisit(IncludeDesc& includeDesc) throw(){
	}

void	ReqCompHeaderVisit::appendToHeaderGuard(const char* name) throw(){
	_headerGuard	+= "_";
	_headerGuard	+= name;
	}

void	ReqCompHeaderVisit::clearMessageFields() throw(){
		InputNode*	inputNode;
		while((inputNode=_inputNodes.get())){
//			delete inputNode;
			}
		OutputNode*	outputNode;
		while((outputNode=_outputNodes.get())){
//			delete outputNode;
			}
		InOutNode*	inOutNode;
		while((inOutNode=_inOutNodes.get())){
			if(!_inOutNodes.first()){
				continue;
				}
//			delete inOutNode;
			}
		_syncDesc	= 0;
		_cancelDesc	= 0;
	}

void	ReqCompHeaderVisit::outputNamespaceSeq() throw(){
	for(	NamespaceNode*	node=_namespaceList.first();
			node;
			node	= _namespaceList.next(node)
			){
		fprintf(	&_outputFile,
					"%s::",
					node->_namespaceDesc.getPrefix().getString()
					);
		}
	}

void	ReqCompHeaderVisit::outputMemberFunctionVariableDeclarations() throw(){
	fprintf(	&_outputFile,
				"\tprivate:\n"
				);

	for(	MessageNode*	messageNode	= _messageNodes.first();
			messageNode;
			messageNode	= _messageNodes.next(messageNode)
			){
		outputMemberFunctionVariableDeclaration(*messageNode);
		}

	for(	CancelNode*	cancelNode	= _cancelNodes.first();
			cancelNode;
			cancelNode	= _cancelNodes.next(cancelNode)
			){
		outputCancelMemberFunctionVariableDeclaration(*cancelNode);
		}
	}

void	ReqCompHeaderVisit::outputConstructorDeclaration() throw(){
	fprintf(	&_outputFile,
				"\tpublic:\n"
				"\t\t/** */\n"
				"\t\tComposer(\n"
				"\t\t\tContext&\t\tcontext"
				);
	for(	MessageNode*	messageNode	= _messageNodes.first();
			messageNode;
			messageNode	= _messageNodes.next(messageNode)
			){
		fprintf(	&_outputFile,
					",\n"
					);
		outputMemberFunctionArgument(*messageNode,"\t\t\t");
		}

	for(	CancelNode*	cancelNode	= _cancelNodes.first();
			cancelNode;
			cancelNode	= _cancelNodes.next(cancelNode)
			){
		fprintf(	&_outputFile,
					",\n"
					);
		outputCancelMemberFunctionArgument(*cancelNode,"\t\t\t");
		}

	fprintf(	&_outputFile,
				"\n\t\t\t) throw();\n\n"
				);

	}

void	ReqCompHeaderVisit::outputMemberFunctionDeclarations() throw(){
	fprintf(	&_outputFile,
				"\tprivate:\n"
				);

	for(	MessageNode*	messageNode	= _messageNodes.first();
			messageNode;
			messageNode	= _messageNodes.next(messageNode)
			){
		outputMemberFunctionDeclaration(*messageNode);
		}

	for(	CancelNode*	cancelNode	= _cancelNodes.first();
			cancelNode;
			cancelNode	= _cancelNodes.next(cancelNode)
			){
		outputCancelMemberFunctionDeclaration(*cancelNode);
		}

	}

void	ReqCompHeaderVisit::outputConstructorImplementation() throw(){
	fprintf(	&_outputFile,
				"template <class Context>\n"
				"Composer<Context>::Composer(\n"
				"\t\t\tContext&\t\tcontext"
				);

	for(	MessageNode*	messageNode	= _messageNodes.first();
			messageNode;
			messageNode	= _messageNodes.next(messageNode)
			){
		fprintf(	&_outputFile,
					",\n"
					);
		outputMemberFunctionArgument(*messageNode,"\t\t\t");
		}

	for(	CancelNode*	cancelNode	= _cancelNodes.first();
			cancelNode;
			cancelNode	= _cancelNodes.next(cancelNode)
			){
		fprintf(	&_outputFile,
					",\n"
					);
		outputCancelMemberFunctionArgument(*cancelNode,"\t\t\t");
		}

	fprintf(	&_outputFile,
				"\n"
				);

	fprintf(	&_outputFile,
				"\t\t\t) throw():\n"
				"\t\t_context(context)"
				);

	for(	MessageNode*	messageNode	= _messageNodes.first();
			messageNode;
			messageNode	= _messageNodes.next(messageNode)
			){
		fprintf(	&_outputFile,
					",\n"
					);
		outputInitializerList(*messageNode);
		}

	for(	CancelNode*	cancelNode	= _cancelNodes.first();
			cancelNode;
			cancelNode	= _cancelNodes.next(cancelNode)
			){
		fprintf(	&_outputFile,
					",\n"
					);
		outputCancelInitializerList(*cancelNode);
		}

	fprintf(	&_outputFile,
				"\n\t\t{\n"
				"\t}\n\n"
				);
	}

void	ReqCompHeaderVisit::outputInitializerList(MessageNode& messageNode) throw(){
	fprintf(	&_outputFile,
				"\t\t_%s(%s)",
				messageNode._messageDesc.getName().getString(),
				messageNode._messageDesc.getName().getString()
				);
	}

void	ReqCompHeaderVisit::outputCancelInitializerList(CancelNode& cancelNode) throw(){
	fprintf(	&_outputFile,
				"\t\t_Cancel%s(Cancel%s)",
				cancelNode._messageDesc.getName().getString(),
				cancelNode._messageDesc.getName().getString()
				);
	}

void	ReqCompHeaderVisit::outputMemberFunctionImplementations() throw(){
	for(	MessageNode*	messageNode	= _messageNodes.first();
			messageNode;
			messageNode	= _messageNodes.next(messageNode)
			){
		outputMemberFunctionImplementation(*messageNode);
		}

	for(	CancelNode*	cancelNode	= _cancelNodes.first();
			cancelNode;
			cancelNode	= _cancelNodes.next(cancelNode)
			){
		outputCancelMemberFunctionImplementation(*cancelNode);
		}

	}

void	ReqCompHeaderVisit::outputMemberFunctionImplementation(
								MessageNode&			messageNode
								) throw(){
	fprintf(	&_outputFile,
				"template <class Context>\n"
				"void\tComposer<Context>::request("
				);

	outputArgumentList(messageNode,"\t\t");

	// Close argument list
	fprintf(	&_outputFile,
				") throw(){\n"
				);

	fprintf(	&_outputFile,
				"\t(_context.*_%s)(",
				messageNode._messageDesc.getName().getString()
				);

	const char*	tabs="\t\t\t\t";

	outputCallArgumentList(messageNode,tabs);

	fprintf(	&_outputFile,
				");\n"
				"\t}\n\n"
				);
	}

void	ReqCompHeaderVisit::outputCancelMemberFunctionImplementation(
								CancelNode&			cancelNode
								) throw(){
	fprintf(	&_outputFile,
				"template <class Context>\n"
				"void\tComposer<Context>::request("
				);

	outputCancelArgumentList(cancelNode,"\t\t");

	// Close argument list
	fprintf(	&_outputFile,
				") throw(){\n"
				);

	fprintf(	&_outputFile,
				"\t(_context.*_Cancel%s)(",
				cancelNode._messageDesc.getName().getString()
				);

	const char*	tabs="\t\t\t\t";

	outputCancelCallArgumentList(cancelNode,tabs);

	fprintf(	&_outputFile,
				");\n"
				"\t}\n\n"
				);
	}

void	ReqCompHeaderVisit::outputMemberFunctionVariableDeclaration(
								MessageNode&			messageNode
								) throw(){
	printComment(	&_outputFile,	// outputFile
					4,				// tabWidthInCharacters
					60,				// paragraphWidthInCharacters
					2,				// nTabIndention
					messageNode._messageDesc.getComment().getString()
					);
	fprintf(	&_outputFile,
				"\t\tvoid\t(Context::*_%s)(",
				messageNode._messageDesc.getName().getString()
				);

	outputArgumentList(messageNode,"\t\t");

	// Close argument list
	fprintf(	&_outputFile,
				");\n\n"
				);
	}

void	ReqCompHeaderVisit::outputCancelMemberFunctionVariableDeclaration(
								CancelNode& cancelNode
								) throw(){
	printComment(	&_outputFile,	// outputFile
					4,				// tabWidthInCharacters
					60,				// paragraphWidthInCharacters
					2,				// nTabIndention
					cancelNode._messageDesc.getComment().getString()
					);
	fprintf(	&_outputFile,
				"\t\tvoid\t(Context::*_Cancel%s)(",
				cancelNode._messageDesc.getName().getString()
				);

	outputCancelArgumentList(cancelNode,"\t\t");

	// Close argument list
	fprintf(	&_outputFile,
				");\n\n"
				);
	}

void	ReqCompHeaderVisit::outputMemberFunctionDeclaration(
								MessageNode&			messageNode
								) throw(){
	printComment(	&_outputFile,	// outputFile
					4,				// tabWidthInCharacters
					60,				// paragraphWidthInCharacters
					2,				// nTabIndention
					messageNode._messageDesc.getComment().getString()
					);
	fprintf(	&_outputFile,
				"\t\tvoid\trequest("
				);

	outputArgumentList(messageNode,"\t\t");

	// Close argument list
	fprintf(	&_outputFile,
				") throw();\n\n"
				);
	}

void	ReqCompHeaderVisit::outputCancelMemberFunctionDeclaration(
								CancelNode&			cancelNode
								) throw(){
	printComment(	&_outputFile,	// outputFile
					4,				// tabWidthInCharacters
					60,				// paragraphWidthInCharacters
					2,				// nTabIndention
					cancelNode._messageDesc.getComment().getString()
					);
	fprintf(	&_outputFile,
				"\t\tvoid\trequest("
				);

	outputCancelArgumentList(cancelNode,"\t\t");

	// Close argument list
	fprintf(	&_outputFile,
				") throw();\n\n"
				);
	}

void	ReqCompHeaderVisit::outputMemberFunctionArgument(
								MessageNode&			messageNode,
								const char*			leadingTabs
								) throw(){
	fprintf(	&_outputFile,
				"%svoid\t(Context::*%s)(",
				leadingTabs,
				messageNode._messageDesc.getName().getString()
				);

	outputArgumentList(messageNode,leadingTabs);

	// Close argument list
	fprintf(	&_outputFile,
				")"
				);
	}

void	ReqCompHeaderVisit::outputCancelMemberFunctionArgument(
								CancelNode&			cancelNode,
								const char*			leadingTabs
								) throw(){
	fprintf(	&_outputFile,
				"%svoid\t(Context::*Cancel%s)(",
				leadingTabs,
				cancelNode._messageDesc.getName().getString()
				);

	outputCancelArgumentList(cancelNode,leadingTabs);

	// Close argument list
	fprintf(	&_outputFile,
				")"
				);
	}

void	ReqCompHeaderVisit::outputArgumentList(
								MessageNode&			messageNode,
								const char*			leadingTabs
								) throw(){
	outputNamespaceSeq();
	fprintf(	&_outputFile,
				"Req::Api::%sReq& msg",
				messageNode._messageDesc.getName().getString()
				);
	}

void	ReqCompHeaderVisit::outputCancelArgumentList(
								CancelNode&			cancelNode,
								const char*			leadingTabs
								) throw(){
	outputNamespaceSeq();
	fprintf(	&_outputFile,
				"Req::Api::Cancel%sReq& msg",
				cancelNode._messageDesc.getName().getString()
				);
	}

void	ReqCompHeaderVisit::outputCallArgumentList(
								MessageNode&			messageNode,
								const char*			leadingTabs
								) throw(){
	fprintf(	&_outputFile,
				"msg"
				);

	}

void	ReqCompHeaderVisit::outputCancelCallArgumentList(
								CancelNode&			cancelNode,
								const char*			leadingTabs
								) throw(){
	fprintf(	&_outputFile,
				"msg"
				);

	}
