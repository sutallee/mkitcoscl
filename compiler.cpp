#include <stdio.h>
#include <string.h>
#include "compiler.h"

using namespace ItcCompiler;

ParseNodeList*	ItcCompiler::theModuleDescList;

ParseNodeList::ParseNodeList() throw(){
	}

void ParseNodeList::accept(ParseElementVisitor& visitor) throw(){
	visitor.preVisit(*this);
	for(ParseNode* node=first();node;node=next(node)){
		node->accept(visitor);
		}
	visitor.postVisit(*this);
	}

CommentDesc::CommentDesc(const char* comment) throw():
		_comment(comment)
		{
	}

void CommentDesc::accept(ParseElementVisitor& visitor) throw(){
//	visitor.preVisit(*this);
//	visitor.postVisit(*this);
	}

const Oscl::Strings::Api&	CommentDesc::getComment() const throw(){
	return _comment;
	}

IncludeDesc::IncludeDesc(
			const char* prefix,
			const char* includeFile,
			const char* postfix
			) throw():
		_includeFile()
		{
	_includeFile	+= prefix;
	_includeFile	+= includeFile;
	_includeFile	+= postfix;
	}

void IncludeDesc::accept(ParseElementVisitor& visitor) throw(){
	visitor.preVisit(*this);
	visitor.postVisit(*this);
	}

const Oscl::Strings::Api&	IncludeDesc::getIncludeFile() const throw(){
	return _includeFile;
	}

NamespaceDesc::NamespaceDesc(char* prefix,ParseNodeList* parseNodeList) throw():
		_parseNodeList(parseNodeList),
		_prefix(prefix)
		{
	}

void NamespaceDesc::accept(ParseElementVisitor& visitor) throw(){
	visitor.preVisit(*this);
	if(_parseNodeList) _parseNodeList->accept(visitor);
	visitor.postVisit(*this);
	}

const Oscl::Strings::Api&	NamespaceDesc::getPrefix() const throw(){
	return _prefix;
	}

ItcDesc::ItcDesc(	const char*		name,
					ParseNodeList*	parseNodeList,
					const char*		comment
					) throw():
		_name(name),
		_parseNodeList(parseNodeList),
		_comment(comment)
		{
	}

void ItcDesc::accept(ParseElementVisitor& visitor) throw(){
	visitor.preVisit(*this);
	if(_parseNodeList) _parseNodeList->accept(visitor);
	visitor.postVisit(*this);
	}

const Oscl::Strings::Api&	ItcDesc::getName() const throw(){
	return _name;
	}

const Oscl::Strings::Api&	ItcDesc::getComment() const throw(){
	return _comment;
	}

SyncDesc::SyncDesc(		const char*			name,
						const char*			field,
						Oscl::Strings::Api*	type,
						const char*			comment
						) throw():
		_name(name),
		_field(field),
		_type(type),
		_comment(comment)
		{
	}

void SyncDesc::accept(ParseElementVisitor& visitor) throw(){
	visitor.preVisit(*this);
	visitor.postVisit(*this);
	}

const Oscl::Strings::Api&	SyncDesc::getName() const throw(){
	return _name;
	}

const Oscl::Strings::Api&	SyncDesc::getField() const throw(){
	return _field;
	}

const Oscl::Strings::Api&	SyncDesc::getType() const throw(){
	return *_type;
	}

const Oscl::Strings::Api&	SyncDesc::getComment() const throw(){
	return _comment;
	}


CancelDesc::CancelDesc(	const char*	comment) throw():
		_comment(comment)
		{
	}

void CancelDesc::accept(ParseElementVisitor& visitor) throw(){
	visitor.preVisit(*this);
	visitor.postVisit(*this);
	}

const Oscl::Strings::Api&	CancelDesc::getComment() const throw(){
	return _comment;
	}

MessageBodyDesc::MessageBodyDesc(	ParseNodeList*	fields,
									ParseNode*		cancelDesc,
									ParseNode*		syncDesc
									) throw():
		_fields(fields),
		_cancelDesc(cancelDesc),
		_syncDesc(syncDesc)
		{
	}

void MessageBodyDesc::accept(ParseElementVisitor& visitor) throw(){
	visitor.preVisit(*this);
	if(_fields) _fields->accept(visitor);
	if(_cancelDesc) _cancelDesc->accept(visitor);
	if(_syncDesc) _syncDesc->accept(visitor);
	visitor.postVisit(*this);
	}

MessageDesc::MessageDesc(	const char*			name,
							MessageBodyDesc&	messageBody,
							const char*			comment
							) throw():
		_name(name),
		_messageBody(messageBody),
		_comment(comment)
		{
	}

void MessageDesc::accept(ParseElementVisitor& visitor) throw(){
	visitor.preVisit(*this);
	_messageBody.accept(visitor);
	visitor.postVisit(*this);
	}

const Oscl::Strings::Api&	MessageDesc::getName() const throw(){
	return _name;
	}

const Oscl::Strings::Api&	MessageDesc::getComment() const throw(){
	return _comment;
	}

InputDesc::InputDesc(		const char*			name,
							Oscl::Strings::Api*	type,
							const char*			comment
							) throw():
		_name(name),
		_type(type),
		_comment(comment)
		{
	}

void InputDesc::accept(ParseElementVisitor& visitor) throw(){
	visitor.preVisit(*this);
	visitor.postVisit(*this);
	}

const Oscl::Strings::Api&	InputDesc::getName() const throw(){
	return _name;
	}

const Oscl::Strings::Api&	InputDesc::getType() const throw(){
	return *_type;
	}

const Oscl::Strings::Api&	InputDesc::getComment() const throw(){
	return _comment;
	}

OutputDesc::OutputDesc(		const char*			name,
							Oscl::Strings::Api*	type,
							const char*			comment,
							const char*			initializer,
							const char*			arraySize
							) throw():
		_name(name),
		_type(type),
		_comment(comment),
		_initializer(initializer),
		_arraySize(arraySize)
		{
	}

void OutputDesc::accept(ParseElementVisitor& visitor) throw(){
	visitor.preVisit(*this);
	visitor.postVisit(*this);
	}

const Oscl::Strings::Api&	OutputDesc::getName() const throw(){
	return _name;
	}

const Oscl::Strings::Api&	OutputDesc::getType() const throw(){
	return *_type;
	}

const Oscl::Strings::Api&	OutputDesc::getComment() const throw(){
	return _comment;
	}

const Oscl::Strings::Api&	OutputDesc::getInitializer() const throw(){
	return _initializer;
	}

const Oscl::Strings::Api&	OutputDesc::getArraySize() const throw(){
	return _arraySize;
	}

InOutDesc::InOutDesc(		const char*			name,
							Oscl::Strings::Api*	type,
							const char*			comment
							) throw():
		_name(name),
		_type(type),
		_comment(comment)
		{
	}

void InOutDesc::accept(ParseElementVisitor& visitor) throw(){
	visitor.preVisit(*this);
	visitor.postVisit(*this);
	}

const Oscl::Strings::Api&	InOutDesc::getName() const throw(){
	return _name;
	}

const Oscl::Strings::Api&	InOutDesc::getType() const throw(){
	return *_type;
	}

const Oscl::Strings::Api&	InOutDesc::getComment() const throw(){
	return _comment;
	}

