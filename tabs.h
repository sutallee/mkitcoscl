#ifndef _tabsh_
#define _tabsh_
#include <stdio.h>
#include "compiler.h"
#include "src/oscl/strings/node.h"
#include "src/oscl/queue/queue.h"

/** */
namespace ItcCompiler {

/** */
class TabsVisit : public ParseElementVisitor {
	private:
		/** */
		Oscl::Queue<Oscl::Strings::Node>	_stack;

	protected:
		/** */
		FILE&								_outputFile;

	public:
		/** */
		TabsVisit() throw();
		/** */
		TabsVisit(FILE& outputFile) throw();

	protected:
		/** */
		void	printStack() throw();
		/** */
		void	pushTab() throw();
		/** */
		void	popTab() throw();
	};

}

#endif
