#include <new>
#include <stdlib.h>
#include <stdio.h>
#include "reqapicpp.h"
#include "comment.h"

using namespace ItcCompiler;

ReqApiCppVisit::ReqApiCppVisit() throw():
		_cancelDesc(0)
		{
	}

ReqApiCppVisit::ReqApiCppVisit(FILE& outputFile) throw():
		TabsVisit(outputFile),
		_cancelDesc(0)
		{
	}

void	ReqApiCppVisit::preVisit(ParseNodeList& parseNodeList) throw(){
	}

void	ReqApiCppVisit::preVisit(NamespaceDesc& namespaceDesc) throw(){
	NamespaceNode*	node	= new NamespaceNode(namespaceDesc);
	_namespaceList.put(node);
	}

void	ReqApiCppVisit::preVisit(ItcDesc& itcDesc) throw(){
	fprintf(	&_outputFile,
				"#include \"reqapi.h\"\n\n"
				);

	if(_namespaceList.first()){
		fprintf(	&_outputFile,
					"using namespace "
					);
		outputNamespaceSeq();
		fprintf(	&_outputFile,
					";\n\n"
					);
		}
	}

void	ReqApiCppVisit::preVisit(SyncDesc& syncDesc) throw(){
	_syncDesc	= &syncDesc;
	}

void	ReqApiCppVisit::preVisit(CancelDesc& cancelDesc) throw(){
	_cancelDesc	= &cancelDesc;
	}

void	ReqApiCppVisit::preVisit(MessageBodyDesc& messageBodyDesc) throw(){
	}

void	ReqApiCppVisit::preVisit(MessageDesc& messageDesc) throw(){
	clearMessageFields();
	MessageNode*	messageNode	= new MessageNode(messageDesc);
	_messageNodes.put(messageNode);
	}

void	ReqApiCppVisit::preVisit(InputDesc& inputDesc) throw(){
	InputNode*	node	= new InputNode(inputDesc);
	_inputNodes.put(node);
	}

void	ReqApiCppVisit::preVisit(OutputDesc& outputDesc) throw(){
	OutputNode*	node	= new OutputNode(outputDesc);
	_outputNodes.put(node);
	}

void	ReqApiCppVisit::preVisit(InOutDesc& inOutDesc) throw(){
	InOutNode*	node	= new InOutNode(inOutDesc);
	_inOutNodes.put(node);
	}

void	ReqApiCppVisit::preVisit(IncludeDesc& includeDesc) throw(){
	IncludeNode*	node	= new IncludeNode(includeDesc);
	_includeNodes.put(node);
	}


void	ReqApiCppVisit::postVisit(ParseNodeList& parseNodeList) throw(){
	}

void	ReqApiCppVisit::postVisit(NamespaceDesc& namespaceDesc) throw(){
	}

void	ReqApiCppVisit::postVisit(ItcDesc& itcDesc) throw(){
	}

void	ReqApiCppVisit::postVisit(SyncDesc& syncDesc) throw(){
	}

void	ReqApiCppVisit::postVisit(CancelDesc& cancelDesc) throw(){
	_cancelDesc	= &cancelDesc;
	}

void	ReqApiCppVisit::postVisit(MessageBodyDesc& messageBodyDesc) throw(){
	}

void	ReqApiCppVisit::postVisit(MessageDesc& messageDesc) throw(){
	outputConstructor(messageDesc);
	if(_cancelDesc){
		outputCancelConstructor(messageDesc);
		}
	_cancelDesc	= 0;
	}

void	ReqApiCppVisit::postVisit(InputDesc& inputDesc) throw(){
	}

void	ReqApiCppVisit::postVisit(OutputDesc& outputDesc) throw(){
	}

void	ReqApiCppVisit::postVisit(InOutDesc& inOutDesc) throw(){
	}

void	ReqApiCppVisit::postVisit(IncludeDesc& includeDesc) throw(){
	}

void	ReqApiCppVisit::clearMessageFields() throw(){
		InputNode*	inputNode;
		while((inputNode=_inputNodes.get())){
//			delete inputNode;
			}
		OutputNode*	outputNode;
		while((outputNode=_outputNodes.get())){
//			delete outputNode;
			}
		InOutNode*	inOutNode;
		while((inOutNode=_inOutNodes.get())){
			if(!_inOutNodes.first()){
				continue;
				}
//			delete inOutNode;
			}
		_syncDesc	= 0;
		_cancelDesc	= 0;
	}

void	ReqApiCppVisit::outputConstructor(MessageDesc& messageDesc) throw(){
	outputNamespaceSeq();
	fprintf(	&_outputFile,
				"::Req::Api::%sPayload::\n"
				"%sPayload(\n",
				messageDesc.getName().getString(),
				messageDesc.getName().getString()
				);
	bool	first = true;
	// Write Constructor Inputs
	InputNode*	inputNode;
	for(	inputNode=_inputNodes.first();
			inputNode;
			inputNode=_inputNodes.next(inputNode)
		){
		if(!first){
			fprintf(&_outputFile,",\n");
			}
		first	= false;
		fprintf(	&_outputFile,
					"\t\t%s\t%s",
					inputNode->_inputDesc.getType().getString(),
					inputNode->_inputDesc.getName().getString()
					);
		}

	// Write Constructor Inputs/Outputs
	InOutNode*	inOutNode;
	for(	inOutNode=_inOutNodes.first();
			inOutNode;
			inOutNode=_inOutNodes.next(inOutNode)
		){
		if(!first){
			fprintf(&_outputFile,",\n");
			}
		first	= false;
		fprintf(	&_outputFile,
					"\t\t%s\t%s",
					inOutNode->_inOutDesc.getType().getString(),
					inOutNode->_inOutDesc.getName().getString()
					);
		}

	if(first){
		fprintf(	&_outputFile,
					"\t\t) throw()"
					);
		}
	else {
		fprintf(	&_outputFile,
					"\n\t\t) throw():\n"
					);
		}

	first = true;
	// Write Constructor Inputs
	for(	inputNode=_inputNodes.first();
			inputNode;
			inputNode=_inputNodes.next(inputNode)
		){
		if(!first){
			fprintf(&_outputFile,",\n");
			}
		first	= false;
		fprintf(	&_outputFile,
					"\t\t_%s(%s)",
					inputNode->_inputDesc.getName().getString(),
					inputNode->_inputDesc.getName().getString()
					);
		}

	// Write Constructor Inputs/Outputs
	for(	inOutNode=_inOutNodes.first();
			inOutNode;
			inOutNode=_inOutNodes.next(inOutNode)
		){
		if(!first){
			fprintf(&_outputFile,",\n");
			}
		first	= false;
		fprintf(	&_outputFile,
					"\t\t_%s(%s)",
					inOutNode->_inOutDesc.getName().getString(),
					inOutNode->_inOutDesc.getName().getString()
					);
		}

	// Write Constructor Outputs
	OutputNode*	outNode;
	for(	outNode=_outputNodes.first();
			outNode;
			outNode=_outputNodes.next(outNode)
		){
		if(outNode->_outputDesc.getInitializer().length()){
			if(first){
				fprintf(&_outputFile,":\n");
				}
			else {
				fprintf(&_outputFile,",\n");
				}
			first	= false;
			fprintf(	&_outputFile,
						"\t\t_%s(%s)",
						outNode->_outputDesc.getName().getString(),
						outNode->_outputDesc.getInitializer().getString()
						);
			}
		}

	fprintf(	&_outputFile,
				"\n\t\t{}\n\n"
				);
	}

void	ReqApiCppVisit::outputNamespaceSeq() throw(){
	bool	first	= true;
	for(	NamespaceNode*	node=_namespaceList.first();
			node;
			node	= _namespaceList.next(node)
			){
		if(!first){
			fprintf(	&_outputFile,
						"::"
						);
			}
		first	= false;
		fprintf(	&_outputFile,
					"%s",
					node->_namespaceDesc.getPrefix().getString()
					);
		}
	}

void	ReqApiCppVisit::outputCancelConstructor(MessageDesc& messageDesc) throw(){
	outputNamespaceSeq();
	fprintf(	&_outputFile,
				"::Req::Api::Cancel%sPayload::\n"
				"Cancel%sPayload(Req::Api::%sReq& requestToCancel) throw():\n"
				"\t\t_requestToCancel(requestToCancel)\n"
				"\t\t{}\n\n",
				messageDesc.getName().getString(),
				messageDesc.getName().getString(),
				messageDesc.getName().getString()
				);
	}
