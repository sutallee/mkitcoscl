#ifndef _compilerh_
#define _compilerh_
#include "oscl/queue/queue.h"
#include "oscl/strings/dynamic.h"
#include "oscl/tree/node.h"

namespace ItcCompiler {

class ParseNodeList;
class NamespaceDesc;
class ItcDesc;
class SyncDesc;
class CancelDesc;
class MessageBodyDesc;
class MessageDesc;
class InputDesc;
class OutputDesc;
class InOutDesc;
class IncludeDesc;

extern ParseNodeList*	theModuleDescList;

class ParseElementVisitor {
	public:
		virtual ~ParseElementVisitor(){}
	public:
		virtual void	preVisit(ParseNodeList& parseNodeList) throw()=0;
		virtual void	preVisit(NamespaceDesc& namespaceDesc) throw()=0;
		virtual void	preVisit(ItcDesc& itcDesc) throw()=0;
		virtual void	preVisit(SyncDesc& syncDesc) throw()=0;
		virtual void	preVisit(CancelDesc& cancelDesc) throw()=0;
		virtual void	preVisit(MessageBodyDesc& messageBodyDesc) throw()=0;
		virtual void	preVisit(MessageDesc& messageDesc) throw()=0;
		virtual void	preVisit(InputDesc& inputDesc) throw()=0;
		virtual void	preVisit(OutputDesc& outputDesc) throw()=0;
		virtual void	preVisit(InOutDesc& inOutDesc) throw()=0;
		virtual void	preVisit(IncludeDesc& includeDesc) throw()=0;
	public:
		virtual void	postVisit(ParseNodeList& parseNodeList) throw()=0;
		virtual void	postVisit(NamespaceDesc& namespaceDesc) throw()=0;
		virtual void	postVisit(ItcDesc& itcDesc) throw()=0;
		virtual void	postVisit(SyncDesc& syncDesc) throw()=0;
		virtual void	postVisit(CancelDesc& cancelDesc) throw()=0;
		virtual void	postVisit(MessageBodyDesc& messageBodyDesc) throw()=0;
		virtual void	postVisit(MessageDesc& messageDesc) throw()=0;
		virtual void	postVisit(InputDesc& inputDesc) throw()=0;
		virtual void	postVisit(OutputDesc& outputDesc) throw()=0;
		virtual void	postVisit(InOutDesc& inOutDesc) throw()=0;
		virtual void	postVisit(IncludeDesc& includeDesc) throw()=0;
	};

/** */
class ParseElementApi {
	public:
		virtual ~ParseElementApi(){}
	public:
		/** */
		virtual void accept(ParseElementVisitor& visitor) throw()=0;
	};

/** */
class ParseNode : public ParseElementApi , public Oscl::QueueItem { };

/** */
class ParseNodeList : public ParseElementApi , public Oscl::Queue<ParseNode> {
	public:
		ParseNodeList() throw();
		void accept(ParseElementVisitor& visitor) throw();
	};

/** */
class ParseList : public ParseElementApi , public Oscl::Queue<ParseNode> {
	public:
		/** */
		ParseList() throw();
		/** */
		void accept(ParseElementVisitor& visitor) throw();
	};

/** */
class IncludeDesc : public ParseNode {
	private:
		/** */
		Oscl::Strings::Dynamic	_includeFile;
	public:
		/** */
		IncludeDesc(
			const char* prefix,
			const char* includeFile,
			const char* postfix
			) throw();
		/** */
		void accept(ParseElementVisitor& visitor) throw();
	public:
		/** */
		const Oscl::Strings::Api&		getIncludeFile() const throw();
	};

/** */
class CommentDesc : public ParseNode {
	private:
		/** */
		Oscl::Strings::Dynamic	_comment;
	public:
		/** */
		CommentDesc(const char* comment) throw();
		/** */
		void accept(ParseElementVisitor& visitor) throw();
	public:
		/** */
		const Oscl::Strings::Api&		getComment() const throw();
	};

/** */
class NamespaceDesc : public ParseNode {
	private:
		/** */
		ParseNodeList*			_parseNodeList;
		/** */
		Oscl::Strings::Dynamic	_prefix;
	public:
		/** */
		NamespaceDesc(char* prefix,ParseNodeList* parseNodeList) throw();
		/** */
		void accept(ParseElementVisitor& visitor) throw();
	public:
		/** */
		const Oscl::Strings::Api&		getPrefix() const throw();
	};

/** */
class ItcDesc : public ParseNode {
	private:
		/** */
		Oscl::Strings::Dynamic	_name;
		/** */
		ParseNodeList*			_parseNodeList;
		/** */
		Oscl::Strings::Dynamic	_comment;
	public:
		/** */
		ItcDesc(	const char*		name,
					ParseNodeList*	parseNodeList,
					const char*		comment
					) throw();
		/** */
		void accept(ParseElementVisitor& visitor) throw();
	public:
		/** */
		const Oscl::Strings::Api&		getName() const throw();
		/** */
		const Oscl::Strings::Api&		getComment() const throw();
	};

/** */
class SyncDesc : public ParseNode {
	private:
		/** */
		Oscl::Strings::Dynamic		_name;
		/** */
		Oscl::Strings::Dynamic		_field;
		/** */
		Oscl::Strings::Api*			_type;
		/** */
		Oscl::Strings::Dynamic		_comment;
	public:
		/** */
		SyncDesc(	const char*			name,
					const char*			field,
					Oscl::Strings::Api*	type,
					const char*			comment
					) throw();
		/** */
		void accept(ParseElementVisitor& visitor) throw();

	public:
		/** */
		const Oscl::Strings::Api&	getName() const throw();
		/** */
		const Oscl::Strings::Api&	getField() const throw();
		/** */
		const Oscl::Strings::Api&	getType() const throw();
		/** */
		const Oscl::Strings::Api&	getComment() const throw();
	};

/** */
class CancelDesc : public ParseNode {
	private:
		/** */
		Oscl::Strings::Dynamic		_comment;
	public:
		/** */
		CancelDesc(const char*	comment) throw();
		/** */
		void accept(ParseElementVisitor& visitor) throw();

	public:
		/** */
		const Oscl::Strings::Api&	getComment() const throw();
	};

/** */
class MessageBodyDesc : public ParseNode {
	private:
		/** */
		ParseNodeList*			_fields;
		/** */
		ParseNode*				_cancelDesc;
		/** */
		ParseNode*				_syncDesc;
	public:
		/** */
		MessageBodyDesc(	ParseNodeList*	fields,
							ParseNode*		cancel,
							ParseNode*		sync
							) throw();
		/** */
		void accept(ParseElementVisitor& visitor) throw();
	};

/** */
class MessageDesc : public ParseNode {
	private:
		/** */
		Oscl::Strings::Dynamic	_name;
		/** */
		MessageBodyDesc&		_messageBody;
		/** */
		Oscl::Strings::Dynamic	_comment;
	public:
		/** */
		MessageDesc(	const char*			name,
						MessageBodyDesc&	messageBody,
						const char*			comment
						) throw();
		/** */
		void accept(ParseElementVisitor& visitor) throw();
	public:
		/** */
		const Oscl::Strings::Api&		getName() const throw();
		/** */
		const Oscl::Strings::Api&		getComment() const throw();
	};

/** */
class InputDesc : public ParseNode {
	private:
		/** */
		Oscl::Strings::Dynamic		_name;
		/** */
		Oscl::Strings::Api*			_type;
		/** */
		Oscl::Strings::Dynamic		_comment;
	public:
		/** */
		InputDesc(	const char*			name,
					Oscl::Strings::Api*	type,
					const char*			comment
					) throw();
		/** */
		void accept(ParseElementVisitor& visitor) throw();

	public:
		/** */
		const Oscl::Strings::Api&	getName() const throw();
		/** */
		const Oscl::Strings::Api&	getType() const throw();
		/** */
		const Oscl::Strings::Api&	getComment() const throw();
	};

/** */
class OutputDesc : public ParseNode {
	private:
		/** */
		Oscl::Strings::Dynamic		_name;
		/** */
		Oscl::Strings::Api*			_type;
		/** */
		Oscl::Strings::Dynamic		_comment;
		/** */
		Oscl::Strings::Dynamic		_initializer;
		/** */
		Oscl::Strings::Dynamic		_arraySize;
	public:
		/** */
		OutputDesc(	const char*			name,
					Oscl::Strings::Api*	type,
					const char*			comment,
					const char*			initializer,
					const char*			arraySize
					) throw();
		/** */
		void accept(ParseElementVisitor& visitor) throw();

	public:
		/** */
		const Oscl::Strings::Api&	getName() const throw();
		/** */
		const Oscl::Strings::Api&	getType() const throw();
		/** */
		const Oscl::Strings::Api&	getComment() const throw();
		/** */
		const Oscl::Strings::Api&	getInitializer() const throw();
		/** */
		const Oscl::Strings::Api&	getArraySize() const throw();
	};

/** */
class InOutDesc : public ParseNode {
	private:
		/** */
		Oscl::Strings::Dynamic		_name;
		/** */
		Oscl::Strings::Api*			_type;
		/** */
		Oscl::Strings::Dynamic		_comment;
	public:
		/** */
		InOutDesc(	const char*			name,
					Oscl::Strings::Api*	type,
					const char*			comment
					) throw();
		/** */
		void accept(ParseElementVisitor& visitor) throw();

	public:
		/** */
		const Oscl::Strings::Api&	getName() const throw();
		/** */
		const Oscl::Strings::Api&	getType() const throw();
		/** */
		const Oscl::Strings::Api&	getComment() const throw();
	};
}

#endif
