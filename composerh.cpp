#include <new>
#include <stdlib.h>
#include <stdio.h>
#include "composerh.h"
#include "comment.h"

using namespace ItcCompiler;

ComposerHeaderVisit::ComposerHeaderVisit() throw()
		{
	}

ComposerHeaderVisit::ComposerHeaderVisit(FILE& outputFile) throw():
		TabsVisit(outputFile)
		{
	}

void	ComposerHeaderVisit::preVisit(ParseNodeList& parseNodeList) throw(){
	}

void	ComposerHeaderVisit::preVisit(NamespaceDesc& namespaceDesc) throw(){
	NamespaceNode*	node	= new NamespaceNode(namespaceDesc);
	_namespaceList.put(node);

	appendToHeaderGuard(namespaceDesc.getPrefix().getString());
	}

void	ComposerHeaderVisit::preVisit(ItcDesc& itcDesc) throw(){
	_headerGuard += "_composerh_";
	_headerGuard.convertToLowerCase();

	fprintf(	&_outputFile,
				"#ifndef %s\n"
				"#define %s\n\n",
				_headerGuard.getString(),
				_headerGuard.getString()
				);

	fprintf(	&_outputFile,
				"#include \"api.h\"\n\n"
				);

	for(	NamespaceNode*	node=_namespaceList.first();
			node;
			node	= _namespaceList.next(node)
			){
		fprintf(	&_outputFile,
					"/** */\n"
					);
		fprintf(	&_outputFile,
					"namespace %s {\n\n",
					node->_namespaceDesc.getPrefix().getString()
					);
		}

	const char	itcGeneralComment[] = {
		"This template class implements a kind of GOF decorator\n"
		"pattern allows the context to employ composition instead\n"
		"of inheritance. Frequently, a context may need to implement\n"
		"more than one interface that may result in method name clashes.\n"
		"This template solves the problem by implementing\n"
		"the interface and then invoking member function\n"
		"pointers in the context instead.\n"
		"One instance of this object is created in the context\n"
		"for each interface of this type used in the context.\n"
		"Each instance is constructed such that it refers to different\n"
		"member functions within the context.\n"
		"The interface of this object is passed to any\n"
		"object that requires the interface. When the\n"
		"object invokes any of the operations of the API,\n"
		"the corresponding member function of the context\n"
		"will subsequently be invoked.\n"
		};
	printComment(	&_outputFile,	// outputFile
					4,				// tabWidthInCharacters
					60,				// paragraphWidthInCharacters
					0,				// nTabIndention
					itcGeneralComment
					);

	fprintf(	&_outputFile,
				"template <class Context>\n"
				"class Composer : public Api {\n"
				);
	}

void	ComposerHeaderVisit::preVisit(SyncDesc& syncDesc) throw(){
	_syncDesc	= &syncDesc;
	}

void	ComposerHeaderVisit::preVisit(CancelDesc& cancelDesc) throw(){
	_cancelDesc	= &cancelDesc;
	}

void	ComposerHeaderVisit::preVisit(MessageBodyDesc& messageBodyDesc) throw(){
	}

void	ComposerHeaderVisit::preVisit(MessageDesc& messageDesc) throw(){
	clearMessageFields();
	MessageNode*	messageNode	= new MessageNode(messageDesc);
	_messageNodes.put(messageNode);
	}

void	ComposerHeaderVisit::preVisit(InputDesc& inputDesc) throw(){
	InputNode*	node	= new InputNode(inputDesc);
	_inputNodes.put(node);
	}

void	ComposerHeaderVisit::preVisit(OutputDesc& outputDesc) throw(){
	OutputNode*	node	= new OutputNode(outputDesc);
	_outputNodes.put(node);
	}

void	ComposerHeaderVisit::preVisit(InOutDesc& inOutDesc) throw(){
	InOutNode*	node	= new InOutNode(inOutDesc);
	_inOutNodes.put(node);
	}

void	ComposerHeaderVisit::preVisit(IncludeDesc& includeDesc) throw(){
	IncludeNode*	node	= new IncludeNode(includeDesc);
	_includeNodes.put(node);
	}


void	ComposerHeaderVisit::postVisit(ParseNodeList& parseNodeList) throw(){
	}

void	ComposerHeaderVisit::postVisit(NamespaceDesc& namespaceDesc) throw(){
//	popTab();
	}

void	ComposerHeaderVisit::postVisit(ItcDesc& itcDesc) throw(){
	// At this point, we have visited all of the nodes within
	// ITC description the parse tree, and have therefore
	// collected all of the information for all of the messages
	// into local data stores.
	// We are now ready to generate all of the remaining
	// output for the class using those data stores.
	// The opening namespace and template class stuff has
	// been written. Now we need to:
	// 1) Write the type typedefs for the member function prototypes.
	//    However, given that is mostly a convenience for the human
	//    developer, we may skip that and just generate the signatures
	//    individually. This would also reduce the number of types
	//    maintained by the compiler.
	// 2) Declare the Context reference member variable.
	// 3) Declare each member function pointer variable.
	// 4) Declare the constructor.
	// 5) Declare each member function.
	// 6) Close the class declaration.
	// 7) Write the constructor implementation.
	// 8) Write each member function implementation.
	// 9) Close up the namespaces.
	// 10) Close up the header guard.

	// Declare the Context reference member variable
	fprintf(	&_outputFile,
				"\tprivate:\n"
				"\t\t/** A reference to the Context.\n"
				"\t\t */\n"
				"\t\tContext&\t_context;\n\n"
				);

	outputMemberFunctionVariableDeclarations();

	outputConstructorDeclaration();

	outputMemberFunctionDeclarations();

	// Close up the "class Api"
	fprintf(	&_outputFile,
				"\t};\n\n"
				);

	outputConstructorImplementation();

	outputMemberFunctionImplementations();

	// Close up the namespaces
	for(	NamespaceNode*	node=_namespaceList.first();
			node;
			node	= _namespaceList.next(node)
			){
		fprintf(	&_outputFile,
					"}\n"
					);
		}

	// Close up the header guard
	fprintf(	&_outputFile,
				"#endif\n"
				);

	}

void	ComposerHeaderVisit::postVisit(SyncDesc& syncDesc) throw(){
	}

void	ComposerHeaderVisit::postVisit(CancelDesc& cancelDesc) throw(){
	}

void	ComposerHeaderVisit::postVisit(MessageBodyDesc& messageBodyDesc) throw(){
	}

void	ComposerHeaderVisit::postVisit(MessageDesc& messageDesc) throw(){
	if(_syncDesc){
		SyncNode*	syncNode	= new SyncNode(messageDesc,*_syncDesc);
		_syncNodes.put(syncNode);
		syncNode->_inputNodes	= _inputNodes;
		syncNode->_outputNodes	= _outputNodes;
		syncNode->_inOutNodes	= _inOutNodes;
		}
	}

void	ComposerHeaderVisit::postVisit(InputDesc& inputDesc) throw(){
	}

void	ComposerHeaderVisit::postVisit(OutputDesc& outputDesc) throw(){
	}

void	ComposerHeaderVisit::postVisit(InOutDesc& inOutDesc) throw(){
	}

void	ComposerHeaderVisit::postVisit(IncludeDesc& includeDesc) throw(){
	}

void	ComposerHeaderVisit::appendToHeaderGuard(const char* name) throw(){
	_headerGuard	+= "_";
	_headerGuard	+= name;
	}

void	ComposerHeaderVisit::clearMessageFields() throw(){
		InputNode*	inputNode;
		while((inputNode=_inputNodes.get())){
//			delete inputNode;
			}
		OutputNode*	outputNode;
		while((outputNode=_outputNodes.get())){
//			delete outputNode;
			}
		InOutNode*	inOutNode;
		while((inOutNode=_inOutNodes.get())){
			if(!_inOutNodes.first()){
				continue;
				}
//			delete inOutNode;
			}
		_syncDesc	= 0;
		_cancelDesc	= 0;
	}

void	ComposerHeaderVisit::outputMemberFunctionVariableDeclarations() throw(){
	fprintf(	&_outputFile,
				"\tprivate:\n"
				);

	for(	SyncNode*	syncNode	= _syncNodes.first();
			syncNode;
			syncNode	= _syncNodes.next(syncNode)
			){
		outputMemberFunctionVariableDeclaration(*syncNode);
		}
	}

void	ComposerHeaderVisit::outputConstructorDeclaration() throw(){
	fprintf(	&_outputFile,
				"\tpublic:\n"
				"\t\t/** */\n"
				"\t\tComposer(\n"
				"\t\t\tContext&\t\tcontext"
				);
	for(	SyncNode*	syncNode	= _syncNodes.first();
			syncNode;
			syncNode	= _syncNodes.next(syncNode)
			){
		fprintf(	&_outputFile,
					",\n"
					);
		outputMemberFunctionArgument(*syncNode,"\t\t\t");
		}

	fprintf(	&_outputFile,
				"\n\t\t\t) throw();\n\n"
				);

	}

void	ComposerHeaderVisit::outputMemberFunctionDeclarations() throw(){
	fprintf(	&_outputFile,
				"\tprivate:\n"
				);

	for(	SyncNode*	syncNode	= _syncNodes.first();
			syncNode;
			syncNode	= _syncNodes.next(syncNode)
			){
		outputMemberFunctionDeclaration(*syncNode);
		}
	}

void	ComposerHeaderVisit::outputConstructorImplementation() throw(){
	fprintf(	&_outputFile,
				"template <class Context>\n"
				"Composer<Context>::Composer(\n"
				"\t\t\tContext&\t\tcontext"
				);

	for(	SyncNode*	syncNode	= _syncNodes.first();
			syncNode;
			syncNode	= _syncNodes.next(syncNode)
			){
		fprintf(	&_outputFile,
					",\n"
					);
		outputMemberFunctionArgument(*syncNode,"\t\t\t");
		}

	fprintf(	&_outputFile,
				"\n"
				);

	fprintf(	&_outputFile,
				"\t\t\t) throw():\n"
				"\t\t_context(context)"
				);

	for(	SyncNode*	syncNode	= _syncNodes.first();
			syncNode;
			syncNode	= _syncNodes.next(syncNode)
			){
		fprintf(	&_outputFile,
					",\n"
					);
		outputInitializerList(*syncNode);
		}

	fprintf(	&_outputFile,
				"\n\t\t{\n"
				"\t}\n\n"
				);
	}

void	ComposerHeaderVisit::outputInitializerList(SyncNode& syncNode) throw(){
	fprintf(	&_outputFile,
				"\t\t_%s(%s)",
				syncNode._syncDesc.getName().getString(),
				syncNode._syncDesc.getName().getString()
				);
	}

void	ComposerHeaderVisit::outputMemberFunctionImplementations() throw(){
	for(	SyncNode*	syncNode	= _syncNodes.first();
			syncNode;
			syncNode	= _syncNodes.next(syncNode)
			){
		outputMemberFunctionImplementation(*syncNode);
		}

	}

void	ComposerHeaderVisit::outputMemberFunctionImplementation(
								SyncNode&			syncNode
								) throw(){
	fprintf(	&_outputFile,
				"template <class Context>\n"
				"%s\tComposer<Context>::%s(\t",
				syncNode._syncDesc.getType().getString(),
				syncNode._syncDesc.getName().getString()
				);

	outputArgumentList(syncNode,"\t\t");

	// Close argument list
	fprintf(	&_outputFile,
				"\n"
				"\t\t\t\t\t\t\t) throw(){\n\n"
				);
//	fprintf(	&_outputFile,
//				"\t//<return> (_context.*_<name>)(<argument list>);\n"
//				);
	fprintf(	&_outputFile,
				"\treturn (_context.*_%s)(",
				syncNode._syncDesc.getName().getString()
				);

	const char*	tabs="\t\t\t\t";

	outputCallArgumentList(syncNode,tabs);

	fprintf(	&_outputFile,
				"\n%s);\n"
				"\t}\n\n",
				tabs
				);
	}

void	ComposerHeaderVisit::outputMemberFunctionVariableDeclaration(
								SyncNode&			syncNode
								) throw(){
	printComment(	&_outputFile,	// outputFile
					4,				// tabWidthInCharacters
					60,				// paragraphWidthInCharacters
					2,				// nTabIndention
					syncNode._syncDesc.getComment().getString()
					);
	fprintf(	&_outputFile,
				"\t\t%s\t(Context::*_%s)(\t",
				syncNode._syncDesc.getType().getString(),
				syncNode._syncDesc.getName().getString()
				);

	outputArgumentList(syncNode,"\t\t");

	// Close argument list
	fprintf(	&_outputFile,
				"\n"
				"\t\t\t\t\t\t\t);\n\n"
				);
	}

void	ComposerHeaderVisit::outputMemberFunctionDeclaration(
								SyncNode&			syncNode
								) throw(){
	printComment(	&_outputFile,	// outputFile
					4,				// tabWidthInCharacters
					60,				// paragraphWidthInCharacters
					2,				// nTabIndention
					syncNode._syncDesc.getComment().getString()
					);
	fprintf(	&_outputFile,
				"\t\t%s\t%s(\t",
				syncNode._syncDesc.getType().getString(),
				syncNode._syncDesc.getName().getString()
				);

	outputArgumentList(syncNode,"\t\t");

	// Close argument list
	fprintf(	&_outputFile,
				"\n"
				"\t\t\t\t\t\t\t) throw();\n\n"
				);
	}

void	ComposerHeaderVisit::outputMemberFunctionArgument(
								SyncNode&			syncNode,
								const char*			leadingTabs
								) throw(){
	fprintf(	&_outputFile,
				"%s%s\t(Context::*%s)(\t",
				leadingTabs,
				syncNode._syncDesc.getType().getString(),
				syncNode._syncDesc.getName().getString()
				);

	outputArgumentList(syncNode,leadingTabs);

	// Close argument list
	fprintf(	&_outputFile,
				"\n"
				"%s\t\t\t\t\t)",
				leadingTabs
				);
	}

void	ComposerHeaderVisit::outputArgumentList(
								SyncNode&			syncNode,
								const char*			leadingTabs
								) throw(){
	bool	first	= true;
	// Write argument list inputs
	InputNode*	inputNode;

	for(	inputNode=syncNode._inputNodes.first();
			inputNode;
			inputNode=syncNode._inputNodes.next(inputNode)
		){
		if(!first){
			fprintf(&_outputFile,",\n");
			}
		else {
			fprintf(&_outputFile,"\n");
			}
		first	= false;
		fprintf(	&_outputFile,
					"%s\t\t\t\t\t%s\t%s",
					leadingTabs,
					inputNode->_inputDesc.getType().getString(),
					inputNode->_inputDesc.getName().getString()
					);
		}

	// Write argument list input/outputs
	InOutNode*	inOutNode;
	for(	inOutNode=syncNode._inOutNodes.first();
			inOutNode;
			inOutNode=syncNode._inOutNodes.next(inOutNode)
		){
		if(!first){
			fprintf(&_outputFile,",\n");
			}
		else {
			fprintf(&_outputFile,"\n");
			}
		first	= false;
		fprintf(	&_outputFile,
					"%s\t\t\t\t\t%s\t%s",
					leadingTabs,
					inOutNode->_inOutDesc.getType().getString(),
					inOutNode->_inOutDesc.getName().getString()
					);
		}

	}

void	ComposerHeaderVisit::outputCallArgumentList(
								SyncNode&			syncNode,
								const char*			leadingTabs
								) throw(){
	bool	first	= true;
	// Write argument list inputs
	InputNode*	inputNode;

	for(	inputNode=syncNode._inputNodes.first();
			inputNode;
			inputNode=syncNode._inputNodes.next(inputNode)
		){
		if(!first){
			fprintf(&_outputFile,",\n");
			}
		else {
			fprintf(&_outputFile,"\n");
			}
		first	= false;
		fprintf(	&_outputFile,
					"%s%s",
					leadingTabs,
					inputNode->_inputDesc.getName().getString()
					);
		}

	// Write argument list input/outputs
	InOutNode*	inOutNode;
	for(	inOutNode=syncNode._inOutNodes.first();
			inOutNode;
			inOutNode=syncNode._inOutNodes.next(inOutNode)
		){
		if(!first){
			fprintf(&_outputFile,",\n");
			}
		else {
			fprintf(&_outputFile,"\n");
			}
		first	= false;
		fprintf(	&_outputFile,
					"%s%s",
					leadingTabs,
					inOutNode->_inOutDesc.getName().getString()
					);
		}

	}

void	ComposerHeaderVisit::outputNamespaceSeq() throw(){
	for(	NamespaceNode*	node=_namespaceList.first();
			node;
			node	= _namespaceList.next(node)
			){
		fprintf(	&_outputFile,
					"%s::",
					node->_namespaceDesc.getPrefix().getString()
					);
		}
	}

