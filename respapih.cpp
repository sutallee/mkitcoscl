#include <new>
#include <stdlib.h>
#include <stdio.h>
#include "respapih.h"
#include "comment.h"

using namespace ItcCompiler;

RespApiHeaderVisit::RespApiHeaderVisit() throw():
		_cancelDesc(0)
		{
	}

RespApiHeaderVisit::RespApiHeaderVisit(FILE& outputFile) throw():
		TabsVisit(outputFile),
		_cancelDesc(0)
		{
	}

void	RespApiHeaderVisit::preVisit(ParseNodeList& parseNodeList) throw(){
	}

void	RespApiHeaderVisit::preVisit(NamespaceDesc& namespaceDesc) throw(){
	NamespaceNode*	node	= new NamespaceNode(namespaceDesc);
	_namespaceList.put(node);

	appendToHeaderGuard(namespaceDesc.getPrefix().getString());

//	pushTab();
	}

void	RespApiHeaderVisit::preVisit(ItcDesc& itcDesc) throw(){
	_headerGuard += "_itc_respapih_";
	_headerGuard.convertToLowerCase();

	fprintf(	&_outputFile,
				"#ifndef %s\n"
				"#define %s\n\n",
				_headerGuard.getString(),
				_headerGuard.getString()
				);

	fprintf(	&_outputFile,
				"#include \"reqapi.h\"\n"
				"#include \"oscl/mt/itc/mbox/clirsp.h\"\n"
				"\n"
				);

	for(	NamespaceNode*	node=_namespaceList.first();
			node;
			node	= _namespaceList.next(node)
			){
		fprintf(	&_outputFile,
					"/** */\n"
					);
		fprintf(	&_outputFile,
					"namespace %s {\n\n",
					node->_namespaceDesc.getPrefix().getString()
					);
		}
	fprintf(	&_outputFile,
				"/** */\n"
				"namespace Resp {\n\n"
				);

	const char	itcGeneralComment[] = {
		"This class describes an ITC client interface. The interface includes\n"
		"the definition of client response messages based on their corresponding\n"
		"server request messages and associated payloads, as well as\n"
		"an interface that is to be implemented by a client that uses the\n"
		"corresponding server asynchronously.\n"
		};
	printComment(	&_outputFile,	// outputFile
					4,				// tabWidthInCharacters
					60,				// paragraphWidthInCharacters
					0,				// nTabIndention
					itcGeneralComment
					);

	printComment(	&_outputFile,	// outputFile
					4,				// tabWidthInCharacters
					60,				// paragraphWidthInCharacters
					0,				// nTabIndention
					itcDesc.getComment().getString()
					);
	fprintf(	&_outputFile,
				"class Api {\n"
				);
	}

void	RespApiHeaderVisit::preVisit(SyncDesc& syncDesc) throw(){
	_syncDesc	= &syncDesc;
	}

void	RespApiHeaderVisit::preVisit(CancelDesc& cancelDesc) throw(){
	_cancelDesc	= &cancelDesc;
	}

void	RespApiHeaderVisit::preVisit(MessageBodyDesc& messageBodyDesc) throw(){
	}

void	RespApiHeaderVisit::preVisit(MessageDesc& messageDesc) throw(){
	clearMessageFields();
	MessageNode*	messageNode	= new MessageNode(messageDesc);
	_messageNodes.put(messageNode);
	}

void	RespApiHeaderVisit::preVisit(InputDesc& inputDesc) throw(){
	InputNode*	node	= new InputNode(inputDesc);
	_inputNodes.put(node);
	}

void	RespApiHeaderVisit::preVisit(OutputDesc& outputDesc) throw(){
	OutputNode*	node	= new OutputNode(outputDesc);
	_outputNodes.put(node);
	}

void	RespApiHeaderVisit::preVisit(InOutDesc& inOutDesc) throw(){
	InOutNode*	node	= new InOutNode(inOutDesc);
	_inOutNodes.put(node);
	}

void	RespApiHeaderVisit::preVisit(IncludeDesc& includeDesc) throw(){
	IncludeNode*	node	= new IncludeNode(includeDesc);
	_includeNodes.put(node);
	}


void	RespApiHeaderVisit::postVisit(ParseNodeList& parseNodeList) throw(){
	}

void	RespApiHeaderVisit::postVisit(NamespaceDesc& namespaceDesc) throw(){
//	popTab();
	}

void	RespApiHeaderVisit::postVisit(ItcDesc& itcDesc) throw(){
	// Write the server Api 
	fprintf(	&_outputFile,
				"\tpublic:\n"
				"\t\t/** Make the compiler happy. */\n"
				"\t\tvirtual ~Api(){}\n\n"
				);
	outputApi();

	// Close up the "class Api"
	fprintf(	&_outputFile,
				"\t};\n\n"
				);

	// Close up the namespaces
	fprintf(	&_outputFile,
				"}\n"
				);
	for(	NamespaceNode*	node=_namespaceList.first();
			node;
			node	= _namespaceList.next(node)
			){
		fprintf(	&_outputFile,
					"}\n"
					);
		}
	// Close up the header guard
	fprintf(	&_outputFile,
				"#endif\n"
				);

	}

void	RespApiHeaderVisit::postVisit(SyncDesc& syncDesc) throw(){
	}

void	RespApiHeaderVisit::postVisit(CancelDesc& cancelDesc) throw(){
	}

void	RespApiHeaderVisit::postVisit(MessageBodyDesc& messageBodyDesc) throw(){
	}

void	RespApiHeaderVisit::postVisit(MessageDesc& messageDesc) throw(){
	fprintf(	&_outputFile,
				"\tpublic:\n"
				);
	outputRespMessage(messageDesc);
	if(_cancelDesc){
		CancelNode*	cancelNode	= new CancelNode(messageDesc);
		_cancelNodes.put(cancelNode);
		outputCancelRespMessage(messageDesc);
		}
	}

void	RespApiHeaderVisit::postVisit(InputDesc& inputDesc) throw(){
	}

void	RespApiHeaderVisit::postVisit(OutputDesc& outputDesc) throw(){
	}

void	RespApiHeaderVisit::postVisit(InOutDesc& inOutDesc) throw(){
	}

void	RespApiHeaderVisit::postVisit(IncludeDesc& includeDesc) throw(){
	}

void	RespApiHeaderVisit::appendToHeaderGuard(const char* name) throw(){
	_headerGuard	+= "_";
	_headerGuard	+= name;
	}

void	RespApiHeaderVisit::clearMessageFields() throw(){
		InputNode*	inputNode;
		while((inputNode=_inputNodes.get())){
//			delete inputNode;
			}
		OutputNode*	outputNode;
		while((outputNode=_outputNodes.get())){
//			delete outputNode;
			}
		InOutNode*	inOutNode;
		while((inOutNode=_inOutNodes.get())){
			if(!_inOutNodes.first()){
				continue;
				}
//			delete inOutNode;
			}
		_syncDesc	= 0;
		_cancelDesc	= 0;
	}

void	RespApiHeaderVisit::outputRespMessage(MessageDesc& messageDesc) throw(){
	fprintf(	&_outputFile,
				"\t\t/**\tThis describes a client response message that corresponds\n"
				"\t\t\tto the %sReq message described in the \"reqapi.h\"\n"
				"\t\t\theader file. This %sResp response message actually\n"
				"\t\t\tcontains a %sReq request message.\n"
				"\t\t */\n",
				messageDesc.getName().getString(),
				messageDesc.getName().getString(),
				messageDesc.getName().getString()
				);
	fprintf(	&_outputFile,
				"\t\ttypedef Oscl::Mt::Itc::CliResponse<\n"
				"\t\t\t\t\t"
				);

	outputNamespaceSeq();

	fprintf(	&_outputFile,
				"Req::Api,\n"
				"\t\t\t\t\tApi,\n"
				"\t\t\t\t\t"
				);

	outputNamespaceSeq();

	fprintf(	&_outputFile,
				"\n"
				"\t\t\t\t\tReq::Api::%sPayload\n"
				"\t\t\t\t\t>\t\t%sResp;\n\n",
				messageDesc.getName().getString(),
				messageDesc.getName().getString()
				);
	}

void	RespApiHeaderVisit::outputApi() throw(){
	for(	MessageNode*	messageNode	= _messageNodes.first();
			messageNode;
			messageNode	= _messageNodes.next(messageNode)
			){
		outputMessageApi(messageNode->_messageDesc);
		}
	for(	CancelNode*	cancelNode	= _cancelNodes.first();
			cancelNode;
			cancelNode	= _cancelNodes.next(cancelNode)
			){
		outputCancelMessageApi(cancelNode->_messageDesc);
		}
	}

void	RespApiHeaderVisit::outputMessageApi(const MessageDesc& messageDesc) throw(){
	fprintf(	&_outputFile,
				"\t\t/** This operation is invoked within the client thread\n"
				"\t\t\twhenever a %sResp message is received.\n"
				"\t\t */\n"
				"\t\tvirtual void\tresponse(\t",
				messageDesc.getName().getString()
				);

	outputNamespaceSeq();

	fprintf(	&_outputFile,
				"\n\t\t\t\t\t\t\t\t\tResp::Api::%sResp& msg\n"
				"\t\t\t\t\t\t\t\t\t) throw()=0;\n\n",
				messageDesc.getName().getString()
				);
	}

void	RespApiHeaderVisit::outputNamespaceSeq() throw(){
	for(	NamespaceNode*	node=_namespaceList.first();
			node;
			node	= _namespaceList.next(node)
			){
		fprintf(	&_outputFile,
					"%s::",
					node->_namespaceDesc.getPrefix().getString()
					);
		}
	}

void	RespApiHeaderVisit::outputCancelRespMessage(MessageDesc& messageDesc) throw(){
	fprintf(	&_outputFile,
				"\t\t/**\tThis describes a client response message that corresponds\n"
				"\t\t\tto the Cancel%sReq message described in the \"reqapi.h\"\n"
				"\t\t\theader file. This Cancel%sResp response message actually\n"
				"\t\t\tcontains a Cancel%sReq request message.\n"
				"\t\t */\n",
				messageDesc.getName().getString(),
				messageDesc.getName().getString(),
				messageDesc.getName().getString()
				);
	fprintf(	&_outputFile,
				"\t\ttypedef Oscl::Mt::Itc::CliResponse<\n"
				"\t\t\t\t\t"
				);

	outputNamespaceSeq();

	fprintf(	&_outputFile,
				"Req::Api,\n"
				"\t\t\t\t\tApi,\n"
				"\t\t\t\t\t"
				);

	outputNamespaceSeq();

	fprintf(	&_outputFile,
				"\n"
				"\t\t\t\t\tReq::Api::Cancel%sPayload\n"
				"\t\t\t\t\t>\t\tCancel%sResp;\n\n",
				messageDesc.getName().getString(),
				messageDesc.getName().getString()
				);
	}

void	RespApiHeaderVisit::outputCancelMessageApi(const MessageDesc& messageDesc) throw(){
	fprintf(	&_outputFile,
				"\t\t/** This operation is invoked within the client thread\n"
				"\t\t\twhenever a Cancel%sResp message is received.\n"
				"\t\t */\n"
				"\t\tvirtual void\tresponse(\t",
				messageDesc.getName().getString()
				);

	outputNamespaceSeq();

	fprintf(	&_outputFile,
				"\n\t\t\t\t\t\t\t\t\tResp::Api::Cancel%sResp& msg\n"
				"\t\t\t\t\t\t\t\t\t) throw()=0;\n\n",
				messageDesc.getName().getString()
				);
	}

