#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include "reqapih.h"
#include "reqapicpp.h"
#include "respapih.h"
#include "respmemh.h"
#include "respmemcpp.h"
#include "apih.h"
#include "composerh.h"
#include "synch.h"
#include "synccpp.h"
#include "reqcomph.h"
#include "respcomph.h"
#include "comment.h"

/////////////////////////////////////////////////////////////////
using namespace ItcCompiler;

int yyparse(void);

class CompileApi {
	public:
		virtual ~CompileApi(){}

		virtual void	compile(	ParseNodeList&	parseNodeList,
									FILE*			outputFile
									) throw()=0;
	};

class ApiHeaderGenerator : public CompileApi {
	public:
		void	compile(	ParseNodeList&	parseNodeList,
							FILE*			outputFile
							) throw();
	};

class ComposerHeaderGenerator : public CompileApi {
	public:
		void	compile(	ParseNodeList&	parseNodeList,
							FILE*			outputFile
							) throw();
	};

class ReqCompHeaderGenerator : public CompileApi {
	public:
		void	compile(	ParseNodeList&	parseNodeList,
							FILE*			outputFile
							) throw();
	};

class RespCompHeaderGenerator : public CompileApi {
	public:
		void	compile(	ParseNodeList&	parseNodeList,
							FILE*			outputFile
							) throw();
	};

class ReqApiHeaderGenerator : public CompileApi {
	public:
		void	compile(	ParseNodeList&	parseNodeList,
							FILE*			outputFile
							) throw();
	};

class ReqApiCppGenerator : public CompileApi {
	public:
		void	compile(	ParseNodeList&	parseNodeList,
							FILE*			outputFile
							) throw();
	};

class RespApiHeaderGenerator : public CompileApi {
	public:
		void	compile(	ParseNodeList&	parseNodeList,
							FILE*			outputFile
							) throw();
	};

class RespMemHeaderGenerator : public CompileApi {
	public:
		void	compile(	ParseNodeList&	parseNodeList,
							FILE*			outputFile
							) throw();
	};

class RespMemCppGenerator : public CompileApi {
	public:
		void	compile(	ParseNodeList&	parseNodeList,
							FILE*			outputFile
							) throw();
	};

class SyncHeaderGenerator : public CompileApi {
	public:
		void	compile(	ParseNodeList&	parseNodeList,
							FILE*			outputFile
							) throw();
	};

class SyncCppGenerator : public CompileApi {
	public:
		void	compile(	ParseNodeList&	parseNodeList,
							FILE*			outputFile
							) throw();
	};

static void printHelp(const char* programName){
	const char	helpFormat[]=	"%s [-h] [-o outputFile] [-s] [-t {api.h,reqapi.h,reqapi.cpp,respapi.h,respmem.h,respmem.cpp,sync.h,sync.cpp}]"
								" inputFile [inputFile,..]\n"
								"This program compiles an ITC description <inputFile> and\n"
								"generates C++ code to implement the ITC interface.\n"
								"options:\n"
								" -h\n"
								"   Displays this help message.\n"
								" -c <copyright_notice>\n"
								"   Specifies the path to file which contains a comment that.\n"
								"   is to be prepended to each generated file. This file is\n"
								"   typically a copyright notice.\n"
								" -t <type>\n"
								"   Generates output based on the value of <type>.\n"
								"   If this option is not specified, then the default\n"
								"   is to generate all <type>s of files.\n"
								"   In that case, any \"-o <outputFile>\" option is ignored.\n"
								"   api.h -\n"
								"     Generates an abstract interface header file for the\n"
								"     synchronous versions of the messages that are defined\n"
								"     with the sync attribute. Unless overriden\n"
								"     by the -o or -s option, the file will be named\n"
								"     \"api.h\".\n"
								"   composer.h -\n"
								"     Generates a Composer template header file for the\n"
								"     synchronous versions of the messages that are defined\n"
								"     with the sync attribute. Unless overriden\n"
								"     by the -o or -s option, the file will be named\n"
								"     \"composer.h\".\n"
								"   reqcomp.h -\n"
								"     Generates a Composer template header file for the\n"
								"     ITC server request interface.\n"
								"     Unless overriden by the -o or -s option, the file\n"
								"     will be named \"reqcomp.h\".\n"
								"   respcomp.h -\n"
								"     Generates a Composer template header file for the\n"
								"     ITC client response interface.\n"
								"     Unless overriden by the -o or -s option, the file\n"
								"     will be named \"respcomp.h\".\n"
								"   reqapi.h -\n"
								"     Generates a header file that describes all of the\n"
								"     ITC server request messages, their corresponding payloads\n"
								"     and the interface to be implemented by the server.\n"
								"     An alias for the SAP is also generated.\n"
								"     Unless overriden by the -o or -s option, the file\n"
								"     will be named \"reqapi.h\".\n"
								"   reqapi.cpp -\n"
								"     Generates a C++ source file that implements the\n"
								"     constructors for the payloads.\n"
								"     Unless overriden by the -o or -s option, the file\n"
								"     will be named \"reqapi.cpp\".\n"
								"   respapi.h -\n"
								"     Generates a header file that describes all of the\n"
								"     ITC client response messages, and the interface to\n"
								"     be implemented by the client.\n"
								"     Unless overriden by the -o or -s option, the file\n"
								"     will be named \"respapi.h\".\n"
								"   respmem.h -\n"
								"     Generates a header file that describes memory allocation\n"
								"     data structures for each client response message.\n"
								"     These data structures are useful for asynchronous\n"
								"     clients.\n"
								"     Unless overriden by the -o or -s option, the file\n"
								"     will be named \"respmem.h\".\n"
								"   respmem.h -\n"
								"     Generates a header file that implements the \"build\"\n"
								"     operations for each client response message.\n"
								"     Unless overriden by the -o or -s option, the file\n"
								"     will be named \"respmem.cpp\".\n"
								"   sync.h -\n"
								"     Generates a header file that describes a concrete class\n"
								"     that implements synchronous ITC for the each ITC message\n"
								"     that includes a \"sync\" field.\n"
								"     Unless overriden by the -o or -s option, the file\n"
								"     will be named \"sync.h\".\n"
								"   sync.cpp -\n"
								"     Generates a C++ file that implements synchronous ITC\n"
								"     for the each ITC message that includes a \"sync\" field.\n"
								"     Unless overriden by the -o or -s option, the file\n"
								"     will be named \"sync.cpp\".\n"
								;
	fprintf(	stderr,
				helpFormat,
				programName
				);
	}

static void compileStream(	const char*		 progName,
							CompileApi&		compiler,
							FILE*			outputFile
							);

static const char*			outputFileName=0;
static const char*			copyrightNotice=0;

static ReqApiHeaderGenerator	reqApiHeaderGenerator;
static ReqApiCppGenerator		reqApiCppGenerator;
static RespApiHeaderGenerator	respApiHeaderGenerator;
static RespMemHeaderGenerator	respMemHeaderGenerator;
static RespMemCppGenerator		respMemCppGenerator;
static ApiHeaderGenerator		apiHeaderGenerator;
static ComposerHeaderGenerator	composerHeaderGenerator;
static ReqCompHeaderGenerator	reqCompHeaderGenerator;
static RespCompHeaderGenerator	respCompHeaderGenerator;
static SyncHeaderGenerator		syncHeaderGenerator;
static SyncCppGenerator			syncCppGenerator;

/////////////////////////////////////////////////////////////////////
static const char* getDefaultFileName(CompileApi* compiler) {
	const char*	defaultFileName	= 0;

	if(compiler == &apiHeaderGenerator){
		defaultFileName	= "api.h";
		}
	if(compiler == &composerHeaderGenerator){
		defaultFileName	= "composer.h";
		}
	if(compiler == &reqCompHeaderGenerator){
		defaultFileName	= "reqcomp.h";
		}
	if(compiler == &respCompHeaderGenerator){
		defaultFileName	= "respcomp.h";
		}
	if(compiler == &reqApiHeaderGenerator){
		defaultFileName	= "reqapi.h";
		}
	if(compiler == &reqApiCppGenerator){
		defaultFileName	= "reqapi.cpp";
		}
	if(compiler == &respApiHeaderGenerator){
		defaultFileName	= "respapi.h";
		}
	if(compiler == &respMemHeaderGenerator){
		defaultFileName	= "respmem.h";
		}
	if(compiler == &respMemCppGenerator){
		defaultFileName	= "respmem.cpp";
		}
	if(compiler == &syncHeaderGenerator){
		defaultFileName	= "sync.h";
		}
	if(compiler == &syncCppGenerator){
		defaultFileName	= "sync.cpp";
		}

	return defaultFileName;
	}

/////////////////////////////////////////////////////////////////////
int main(int argc,char *argv[]){
	int						opt;
	bool					useStdIn=false;
	CompileApi*				compiler = 0;
	extern FILE*			yyin;
	bool					useStdout=false;

	while((opt = getopt(argc,argv,"sit:o:hc:")) != -1){
		switch(opt){
			case 'c':
				copyrightNotice	= optarg;
				break;
			case 's':
				outputFileName	= 0;
				useStdout		= true;
				break;
			case 't':
				if(strcmp("api.h",optarg) == 0){
					compiler	= &apiHeaderGenerator;
					}
				else if(strcmp("composer.h",optarg) == 0){
					compiler	= &composerHeaderGenerator;
					}
				else if(strcmp("reqcomp.h",optarg) == 0){
					compiler	= &reqCompHeaderGenerator;
					}
				else if(strcmp("respcomp.h",optarg) == 0){
					compiler	= &respCompHeaderGenerator;
					}
				else if(strcmp("reqapi.h",optarg) == 0){
					compiler	= &reqApiHeaderGenerator;
					}
				else if(strcmp("reqapi.cpp",optarg) == 0){
					compiler	= &reqApiCppGenerator;
					}
				else if(strcmp("respapi.h",optarg) == 0){
					compiler	= &respApiHeaderGenerator;
					}
				else if(strcmp("respmem.h",optarg) == 0){
					compiler	= &respMemHeaderGenerator;
					}
				else if(strcmp("respmem.cpp",optarg) == 0){
					compiler	= &respMemCppGenerator;
					}
				else if(strcmp("sync.h",optarg) == 0){
					compiler	= &syncHeaderGenerator;
					}
				else if(strcmp("sync.cpp",optarg) == 0){
					compiler	= &syncCppGenerator;
					}
				else {
					fprintf(	stderr,
								"%s: unrecognized output type %s\n",
								argv[0],
								optarg
								);
					exit(1);
					}
				break;
			case 'o':
				outputFileName	= optarg;
				useStdout		= false;
				break;
			case 'h':
				printHelp(argv[0]);
				exit(1);
			case 'i':
				useStdIn	= true;
				break;
			case ':':
				fprintf(stderr,"%s: option %c requires an argument\n",argv[0],optopt);
				exit(1);
			case '?':
				fprintf(stderr,"%s: unknown option: %c\n",argv[0],optopt);
				exit(1);
			}
		}

	if(!outputFileName & !useStdout){
		outputFileName	= getDefaultFileName(compiler);
		}

	FILE*	outputFile;
	if(outputFileName && compiler){
		outputFile	= fopen(outputFileName,"w");
		if(!outputFile){
			fprintf(stderr,"%s: can't open output file!\n",argv[0]);
			perror(argv[0]);
			exit(1);
			}
		}
	else {
		outputFile	= stdout;
		}

	if(useStdIn){
		yyin	= stdin;
//		compileStream(argv[0],*compiler,outputFile);
		}
	else if(optind >= argc){
		fprintf(	stderr,
					"%s: missing input file(s)!\n",
					argv[0]
					);
		printHelp(argv[0]);
		}
	else {
		yyin	= fopen(argv[optind],"r");
		if(!yyin){
			fprintf(	stderr,
						"%s: can't open input file %s\n",
						argv[0],
						argv[optind]
						);
			perror(argv[0]);
			exit(1);
			}
		}

	// Now parse the input file to build the
	// parse tree theModuleDescList.
	int	status	= yyparse();

	if(status){
		fprintf(	stderr,
					"%s: yyparse() failed! : %d\n",
					argv[0],
					status
					);
		exit(1);
		}

	if(!compiler){
		// No specific type was specified, therefore,
		// do them all!
		CompileApi*	allCompilers[] = {
			&reqApiHeaderGenerator,
			&reqApiCppGenerator,
			&respApiHeaderGenerator,
			&respMemHeaderGenerator,
			&respMemCppGenerator,
			&apiHeaderGenerator,
			&composerHeaderGenerator,
			&reqCompHeaderGenerator,
			&respCompHeaderGenerator,
			&syncHeaderGenerator,
			&syncCppGenerator
			};
		for(unsigned i=0;i<(sizeof(allCompilers)/sizeof(CompileApi*));++i){
			const char*	filename	= getDefaultFileName(allCompilers[i]);
			if(!filename){
				// If the compiler doesn't have a default name,
				// then we can't generate the output for it.
				continue;
				}
			FILE*
			file	= fopen(filename,"w");
			if(!file){
				fprintf(stderr,"%s: can't open output file: %s!\n",argv[0],filename);
				perror(argv[0]);
				exit(1);
				}
			compileStream(argv[0],*allCompilers[i],file);
			fclose(file);
			}
		}
	else {
		compileStream(argv[0],*compiler,outputFile);
		}

	return 0;
	}

static void compileStream(	const char*		 progName,
							CompileApi&		compiler,
							FILE*			outputFile
							){
	if(copyrightNotice){
		FILE*	cn	= fopen(copyrightNotice,"r");
		if(!cn){
			perror("Trying to open copyright notice file failed:");
			fprintf(	stderr,
						"%s: cannot open copyright notice file %s.\n",
						progName,
						copyrightNotice
						);
			return;
			}

		char	buffer[1024];
		char*	p;
		while((p=fgets(buffer,sizeof(buffer)-1,cn))){
			fputs(p,outputFile);
			}
		}

	compiler.compile(*theModuleDescList,outputFile);
//	delete theModuleDescList;
//	theModuleDescList	= 0;
	}

/////////////////////////////////////////////////////////////////
void	ApiHeaderGenerator::compile(	ParseNodeList&	parseNodeList,
										FILE*			outputFile
										) throw(){
	ApiHeaderVisit	p6(*outputFile);
	parseNodeList.accept(p6);
	}

/////////////////////////////////////////////////////////////////
void	ComposerHeaderGenerator::compile(	ParseNodeList&	parseNodeList,
											FILE*			outputFile
											) throw(){
	ComposerHeaderVisit	composerHeader(*outputFile);
	parseNodeList.accept(composerHeader);
	}

/////////////////////////////////////////////////////////////////
void	ReqCompHeaderGenerator::compile(	ParseNodeList&	parseNodeList,
											FILE*			outputFile
											) throw(){
	ReqCompHeaderVisit	reqCompHeader(*outputFile);
	parseNodeList.accept(reqCompHeader);
	}

/////////////////////////////////////////////////////////////////
void	RespCompHeaderGenerator::compile(	ParseNodeList&	parseNodeList,
											FILE*			outputFile
											) throw(){
	RespCompHeaderVisit	respCompHeader(*outputFile);
	parseNodeList.accept(respCompHeader);
	}

/////////////////////////////////////////////////////////////////
void	ReqApiHeaderGenerator::compile(	ParseNodeList&	parseNodeList,
										FILE*			outputFile
										) throw(){
	ReqApiHeaderVisit	p1(*outputFile);
	parseNodeList.accept(p1);
	}

/////////////////////////////////////////////////////////////////
void	ReqApiCppGenerator::compile(	ParseNodeList&	parseNodeList,
										FILE*			outputFile
										) throw(){
	ReqApiCppVisit	p2(*outputFile);
	parseNodeList.accept(p2);
	}

/////////////////////////////////////////////////////////////////
void	RespApiHeaderGenerator::compile(	ParseNodeList&	parseNodeList,
											FILE*			outputFile
											) throw(){
	RespApiHeaderVisit	p3(*outputFile);
	parseNodeList.accept(p3);
	}

/////////////////////////////////////////////////////////////////
void	RespMemHeaderGenerator::compile(	ParseNodeList&	parseNodeList,
											FILE*			outputFile
											) throw(){
	RespMemHeaderVisit	p4(*outputFile);
	parseNodeList.accept(p4);
	}

/////////////////////////////////////////////////////////////////
void	RespMemCppGenerator::compile(	ParseNodeList&	parseNodeList,
										FILE*			outputFile
										) throw(){
	RespMemCppVisit	p5(*outputFile);
	parseNodeList.accept(p5);
	}

/////////////////////////////////////////////////////////////////
void	SyncHeaderGenerator::compile(	ParseNodeList&	parseNodeList,
										FILE*			outputFile
										) throw(){
	SyncHeaderVisit	p7(*outputFile);
	parseNodeList.accept(p7);
	}

/////////////////////////////////////////////////////////////////
void	SyncCppGenerator::compile(	ParseNodeList&	parseNodeList,
									FILE*			outputFile
									) throw(){
	SyncCppVisit	p8(*outputFile);
	parseNodeList.accept(p8);
	}

