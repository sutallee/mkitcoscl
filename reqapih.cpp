#include <new>
#include <stdlib.h>
#include <stdio.h>
#include "reqapih.h"
#include "comment.h"

using namespace ItcCompiler;

ReqApiHeaderVisit::ReqApiHeaderVisit() throw()
		{
	}

ReqApiHeaderVisit::ReqApiHeaderVisit(FILE& outputFile) throw():
		TabsVisit(outputFile)
		{
	}

void	ReqApiHeaderVisit::preVisit(ParseNodeList& parseNodeList) throw(){
	}

void	ReqApiHeaderVisit::preVisit(NamespaceDesc& namespaceDesc) throw(){
	NamespaceNode*	node	= new NamespaceNode(namespaceDesc);
	_namespaceList.put(node);

	appendToHeaderGuard(namespaceDesc.getPrefix().getString());
	}

void	ReqApiHeaderVisit::preVisit(ItcDesc& itcDesc) throw(){
	_headerGuard += "_itc_reqapih_";
	_headerGuard.convertToLowerCase();

	fprintf(	&_outputFile,
				"#ifndef %s\n"
				"#define %s\n\n",
				_headerGuard.getString(),
				_headerGuard.getString()
				);

	fprintf(	&_outputFile,
				"#include \"oscl/mt/itc/mbox/srvreq.h\"\n"
				"#include \"oscl/mt/itc/postmsgapi.h\"\n"
				"#include \"oscl/mt/itc/mbox/sap.h\"\n"
				);

	for(	IncludeNode*	node=_includeNodes.first();
			node;
			node	= _includeNodes.next(node)
			){
		fprintf(	&_outputFile,
			"#include %s\n",
			node->_includeDesc.getIncludeFile().getString()
			);
		}

	fprintf(	&_outputFile,
			"\n"
			);

	for(	NamespaceNode*	node=_namespaceList.first();
			node;
			node	= _namespaceList.next(node)
			){
		fprintf(	&_outputFile,
					"/** */\n"
					);
		fprintf(	&_outputFile,
					"namespace %s {\n\n",
					node->_namespaceDesc.getPrefix().getString()
					);
		}
	fprintf(	&_outputFile,
				"/** */\n"
				"namespace Req {\n\n"
				);

	const char	itcGeneralComment[] = {
		"This class describes an ITC server interface. The interface includes\n"
		"the definition of messages and their associated payloads, as well as\n"
		"an interface that is to be implemented by a server that supports this\n"
		"ITC interface.\n"
		};
	printComment(	&_outputFile,	// outputFile
					4,				// tabWidthInCharacters
					60,				// paragraphWidthInCharacters
					0,				// nTabIndention
					itcGeneralComment
					);

	printComment(	&_outputFile,	// outputFile
					4,				// tabWidthInCharacters
					60,				// paragraphWidthInCharacters
					0,				// nTabIndention
					itcDesc.getComment().getString()
					);
	fprintf(	&_outputFile,
				"class Api {\n"
				);
	}

void	ReqApiHeaderVisit::preVisit(SyncDesc& syncDesc) throw(){
	_syncDesc	= &syncDesc;
	}

void	ReqApiHeaderVisit::preVisit(CancelDesc& cancelDesc) throw(){
	_cancelDesc	= &cancelDesc;
	}

void	ReqApiHeaderVisit::preVisit(MessageBodyDesc& messageBodyDesc) throw(){
	}

void	ReqApiHeaderVisit::preVisit(MessageDesc& messageDesc) throw(){
	clearMessageFields();
	MessageNode*	messageNode	= new MessageNode(messageDesc);
	_messageNodes.put(messageNode);
	}

void	ReqApiHeaderVisit::preVisit(InputDesc& inputDesc) throw(){
	InputNode*	node	= new InputNode(inputDesc);
	_inputNodes.put(node);
	}

void	ReqApiHeaderVisit::preVisit(OutputDesc& outputDesc) throw(){
	OutputNode*	node	= new OutputNode(outputDesc);
	_outputNodes.put(node);
	}

void	ReqApiHeaderVisit::preVisit(InOutDesc& inOutDesc) throw(){
	InOutNode*	node	= new InOutNode(inOutDesc);
	_inOutNodes.put(node);
	}

void	ReqApiHeaderVisit::preVisit(IncludeDesc& includeDesc) throw(){
	IncludeNode*	node	= new IncludeNode(includeDesc);
	_includeNodes.put(node);
	}


void	ReqApiHeaderVisit::postVisit(ParseNodeList& parseNodeList) throw(){
	}

void	ReqApiHeaderVisit::postVisit(NamespaceDesc& namespaceDesc) throw(){
//	popTab();
	}

void	ReqApiHeaderVisit::postVisit(ItcDesc& itcDesc) throw(){
	// Write the SAP alias
	outputSAP();

	// Write the server Api 
	outputApi();

	// Close up the "class Api"
	fprintf(	&_outputFile,
				"\t};\n\n"
				);

	// Close up the namespaces
	fprintf(	&_outputFile,
				"}\n"
				);
	for(	NamespaceNode*	node=_namespaceList.first();
			node;
			node	= _namespaceList.next(node)
			){
		fprintf(	&_outputFile,
					"}\n"
					);
		}
	// Close up the header guard
	fprintf(	&_outputFile,
				"#endif\n"
				);

	}

void	ReqApiHeaderVisit::postVisit(SyncDesc& syncDesc) throw(){
	}

void	ReqApiHeaderVisit::postVisit(CancelDesc& cancelDesc) throw(){
	}

void	ReqApiHeaderVisit::postVisit(MessageBodyDesc& messageBodyDesc) throw(){
	}

void	ReqApiHeaderVisit::postVisit(MessageDesc& messageDesc) throw(){
	fprintf(	&_outputFile,
				"\tpublic:\n"
				);
	outputPayload(messageDesc);
	outputReqMessage(messageDesc);
	if(_cancelDesc){
		CancelNode*	cancelNode	= new CancelNode(messageDesc);
		_cancelNodes.put(cancelNode);
		outputCancelPayload(messageDesc);
		outputCancelReqMessage(messageDesc);
		}
	}

void	ReqApiHeaderVisit::postVisit(InputDesc& inputDesc) throw(){
	}

void	ReqApiHeaderVisit::postVisit(OutputDesc& outputDesc) throw(){
	}

void	ReqApiHeaderVisit::postVisit(InOutDesc& inOutDesc) throw(){
	}

void	ReqApiHeaderVisit::postVisit(IncludeDesc& includeDesc) throw(){
	}

void	ReqApiHeaderVisit::appendToHeaderGuard(const char* name) throw(){
	_headerGuard	+= "_";
	_headerGuard	+= name;
	}

void	ReqApiHeaderVisit::clearMessageFields() throw(){
		InputNode*	inputNode;
		while((inputNode=_inputNodes.get())){
//			delete inputNode;
			}
		OutputNode*	outputNode;
		while((outputNode=_outputNodes.get())){
//			delete outputNode;
			}
		InOutNode*	inOutNode;
		while((inOutNode=_inOutNodes.get())){
			if(!_inOutNodes.first()){
				continue;
				}
//			delete inOutNode;
			}
		_syncDesc	= 0;
		_cancelDesc	= 0;
	}

void	ReqApiHeaderVisit::outputPayload(MessageDesc& messageDesc) throw(){
	// Open Payload Class
	fprintf(	&_outputFile,
				"\t\t/** This class contains the data that is transfered\n"
				"\t\t\tbetween the client and server within the %sReq\n"
				"\t\t\tITC message.\n"
				"\t\t */\n",
				messageDesc.getName().getString()
				);
	fprintf(	&_outputFile,
				"\t\tclass %sPayload {\n"
				"\t\t\tpublic:\n",
				messageDesc.getName().getString()
				);

	// Write member variables
	// Write Inputs
	InputNode*	inputNode;
	for(	inputNode=_inputNodes.first();
			inputNode;
			inputNode=_inputNodes.next(inputNode)
		){
		printComment(	&_outputFile,	// outputFile
						4,				// tabWidthInCharacters
						60,				// paragraphWidthInCharacters
						4,				// nTabIndention
						inputNode->_inputDesc.getComment().getString()
						);
		fprintf(	&_outputFile,
					"\t\t\t\t%s\t_%s;\n\n",
					inputNode->_inputDesc.getType().getString(),
					inputNode->_inputDesc.getName().getString()
					);
		}

	// Write Inputs/Outputs
	InOutNode*	inOutNode;
	for(	inOutNode=_inOutNodes.first();
			inOutNode;
			inOutNode=_inOutNodes.next(inOutNode)
		){
		printComment(	&_outputFile,	// outputFile
						4,				// tabWidthInCharacters
						60,				// paragraphWidthInCharacters
						4,				// nTabIndention
						inOutNode->_inOutDesc.getComment().getString()
						);
		fprintf(	&_outputFile,
					"\t\t\t\t%s\t_%s;\n\n",
					inOutNode->_inOutDesc.getType().getString(),
					inOutNode->_inOutDesc.getName().getString()
					);
		}

	// Write Outputs
	OutputNode*	outputNode;
	for(	outputNode=_outputNodes.first();
			outputNode;
			outputNode=_outputNodes.next(outputNode)
		){
		printComment(	&_outputFile,	// outputFile
						4,				// tabWidthInCharacters
						60,				// paragraphWidthInCharacters
						4,				// nTabIndention
						outputNode->_outputDesc.getComment().getString()
						);
		fprintf(	&_outputFile,
					"\t\t\t\t%s\t_%s;\n\n",
					outputNode->_outputDesc.getType().getString(),
					outputNode->_outputDesc.getName().getString()
					);
		}

	// Open Constructor
	fprintf(	&_outputFile,
				"\t\t\tpublic:\n"
				"\t\t\t\t/** The %sPayload constructor. */\n"
				"\t\t\t\t%sPayload(\n",
				messageDesc.getName().getString(),
				messageDesc.getName().getString()
				);

	bool	first	= true;
	// Write Constructor Inputs
	for(	inputNode=_inputNodes.first();
			inputNode;
			inputNode=_inputNodes.next(inputNode)
		){
		if(!first){
			fprintf(&_outputFile,",\n");
			}
		first	= false;
		fprintf(	&_outputFile,
					"\n\t\t\t\t\t%s\t%s",
					inputNode->_inputDesc.getType().getString(),
					inputNode->_inputDesc.getName().getString()
					);
		}

	// Write Constructor Inputs/Outputs
	for(	inOutNode=_inOutNodes.first();
			inOutNode;
			inOutNode=_inOutNodes.next(inOutNode)
		){
		if(!first){
			fprintf(&_outputFile,",\n");
			}
		first	= false;
		fprintf(	&_outputFile,
					"\t\t\t\t\t%s\t%s",
					inOutNode->_inOutDesc.getType().getString(),
					inOutNode->_inOutDesc.getName().getString()
					);
		}

	// Close Constructor
	if(!first){
		fprintf(&_outputFile,"\n");
		}

	fprintf(	&_outputFile,
				"\t\t\t\t\t) throw();\n\n"
				);

	// Close the payload
	fprintf(	&_outputFile,
				"\t\t\t};\n\n"
				);

	}

void	ReqApiHeaderVisit::outputReqMessage(MessageDesc& messageDesc) throw(){
	printComment(	&_outputFile,	// outputFile
					4,				// tabWidthInCharacters
					60,				// paragraphWidthInCharacters
					2,				// nTabIndention
					messageDesc.getComment().getString()
					);
	fprintf(	&_outputFile,
				"\t\ttypedef Oscl::Mt::Itc::SrvRequest<\n"
				"\t\t\t\t\t"
				);
	outputNamespaceSeq();

	fprintf(	&_outputFile,
				"Req::Api,\n"
				"\t\t\t\t\t%sPayload\n"
				"\t\t\t\t\t>\t\t%sReq;\n\n",
				messageDesc.getName().getString(),
				messageDesc.getName().getString()
				);

	}

void	ReqApiHeaderVisit::outputApi() throw(){
	fprintf(	&_outputFile,
				"\tpublic:\n"
				"\t\t/** Make the compiler happy. */\n"
				"\t\tvirtual ~Api(){}\n\n"
				);
	for(	MessageNode*	messageNode	= _messageNodes.first();
			messageNode;
			messageNode	= _messageNodes.next(messageNode)
			){
		outputMessageApi(messageNode->_messageDesc);
		}

	for(	CancelNode*	cancelNode	= _cancelNodes.first();
			cancelNode;
			cancelNode	= _cancelNodes.next(cancelNode)
			){
		outputCancelMessageApi(cancelNode->_messageDesc);
		}
	}

void	ReqApiHeaderVisit::outputMessageApi(const MessageDesc& messageDesc) throw(){
	fprintf(	&_outputFile,
				"\t\t/** This operation is invoked within the server thread\n"
				"\t\t\twhenever a %sReq is received.\n"
				"\t\t */\n"
				"\t\tvirtual void\trequest(\t",
				messageDesc.getName().getString()
				);

	outputNamespaceSeq();

	fprintf(	&_outputFile,
				"\n"
				"\t\t\t\t\t\t\t\t\tReq::Api::%sReq& msg\n"
				"\t\t\t\t\t\t\t\t\t) throw()=0;\n\n",
				messageDesc.getName().getString()
				);
	}

void	ReqApiHeaderVisit::outputNamespaceSeq() throw(){
	for(	NamespaceNode*	node=_namespaceList.first();
			node;
			node	= _namespaceList.next(node)
			){
		fprintf(	&_outputFile,
					"%s::",
					node->_namespaceDesc.getPrefix().getString()
					);
		}
	}

void	ReqApiHeaderVisit::outputSAP() throw(){
		/** This type defines a SAP type for this ITC interface.
			A SAP is used to hold server address information that is
			required by clients to send messages to a server.
		 */
	fprintf(	&_outputFile,
				"\tpublic:\n"
				"\t\t/** This type defines a SAP (Service Access Point) for this\n"
				"\t\t\tITC server interface. A SAP holds server address information\n"
				"\t\t\tthat is required by clients to send messages to a server.\n"
				"\t\t */\n"
				"\t\ttypedef Oscl::Mt::Itc::SAP<\t"
				);

	outputNamespaceSeq();

	fprintf(	&_outputFile,
				"\n"
				"\t\t\t\t\t\t\tReq::Api\n"
				"\t\t\t\t\t\t\t>\t\t\tSAP;\n\n"
				);

	fprintf(	&_outputFile,
				"\tpublic:\n"
				"\t\t/** This type defines a SAP (Service Access Point) for this\n"
				"\t\t\tITC server interface. A SAP holds server address information\n"
				"\t\t\tthat is required by clients to send messages to a server.\n"
				"\t\t */\n"
				"\t\ttypedef Oscl::Mt::Itc::ConcreteSAP<\t"
				);

	outputNamespaceSeq();

	fprintf(	&_outputFile,
				"\n"
				"\t\t\t\t\t\t\tReq::Api\n"
				"\t\t\t\t\t\t\t>\t\t\tConcreteSAP;\n\n"
				);
	}

void	ReqApiHeaderVisit::outputCancelPayload(MessageDesc& messageDesc) throw(){
	// Open Payload Class
	fprintf(	&_outputFile,
				"\tpublic:\n"
				"\t\t/** This class contains the data that is transfered\n"
				"\t\t\tbetween the client and server within the Cancel%sReq\n"
				"\t\t\tITC message.\n"
				"\t\t */\n",
				messageDesc.getName().getString()
				);

	fprintf(	&_outputFile,
				"\t\tclass Cancel%sPayload {\n"
				"\t\t\tpublic:\n",
				messageDesc.getName().getString()
				);

	fprintf(	&_outputFile,
				"\t\t\t\t%sReq&\t_requestToCancel;\n\n",
				messageDesc.getName().getString()
				);

	// Open Constructor
	fprintf(	&_outputFile,
				"\t\t\tpublic:\n"
				"\t\t\t\t/** The Cancel%sPayload constructor. */\n"
				"\t\t\t\tCancel%sPayload(%sReq& requestToCancel) throw();\n"
				,
				messageDesc.getName().getString(),
				messageDesc.getName().getString(),
				messageDesc.getName().getString()
				);

	// Close the payload
	fprintf(	&_outputFile,
				"\t\t\t};\n\n"
				);

	}

void	ReqApiHeaderVisit::outputCancelReqMessage(MessageDesc& messageDesc) throw(){
	fprintf(	&_outputFile,
				"\t\t/**\tThis is a message used to cancel a %sReq.\n"
				"\t\t */\t\n"
				"\t\ttypedef Oscl::Mt::Itc::SrvRequest<\n"
				"\t\t\t\t\t",
				messageDesc.getName().getString()
				);

	outputNamespaceSeq();

	fprintf(	&_outputFile,
				"Req::Api,\n"
				"\t\t\t\t\tCancel%sPayload\n"
				"\t\t\t\t\t>\t\tCancel%sReq;\n\n",
				messageDesc.getName().getString(),
				messageDesc.getName().getString()
				);

	}

void	ReqApiHeaderVisit::outputCancelMessageApi(const MessageDesc& messageDesc) throw(){
	fprintf(	&_outputFile,
				"\t\t/** This operation is invoked within the server thread\n"
				"\t\t\twhenever a Cancel%sReq is received.\n"
				"\t\t */\n"
				"\t\tvirtual void\trequest(\t",
				messageDesc.getName().getString()
				);

	outputNamespaceSeq();

	fprintf(	&_outputFile,
				"\n"
				"\t\t\t\t\t\t\t\t\tReq::Api::Cancel%sReq& msg\n"
				"\t\t\t\t\t\t\t\t\t) throw()=0;\n\n",
				messageDesc.getName().getString()
				);
	}

