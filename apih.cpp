#include <new>
#include <stdlib.h>
#include <stdio.h>
#include "apih.h"
#include "comment.h"

using namespace ItcCompiler;

ApiHeaderVisit::ApiHeaderVisit() throw()
		{
	}

ApiHeaderVisit::ApiHeaderVisit(FILE& outputFile) throw():
		TabsVisit(outputFile)
		{
	}

void	ApiHeaderVisit::preVisit(ParseNodeList& parseNodeList) throw(){
	}

void	ApiHeaderVisit::preVisit(NamespaceDesc& namespaceDesc) throw(){
	NamespaceNode*	node	= new NamespaceNode(namespaceDesc);
	_namespaceList.put(node);

	appendToHeaderGuard(namespaceDesc.getPrefix().getString());
	}

void	ApiHeaderVisit::preVisit(ItcDesc& itcDesc) throw(){
	_headerGuard += "_apih_";
	_headerGuard.convertToLowerCase();

	fprintf(	&_outputFile,
				"#ifndef %s\n"
				"#define %s\n\n",
				_headerGuard.getString(),
				_headerGuard.getString()
				);

	for(	IncludeNode*	node=_includeNodes.first();
			node;
			node	= _includeNodes.next(node)
			){
		fprintf(	&_outputFile,
			"#include %s\n",
			node->_includeDesc.getIncludeFile().getString()
			);
		}

	fprintf(	&_outputFile,
			"\n"
			);

	for(	NamespaceNode*	node=_namespaceList.first();
			node;
			node	= _namespaceList.next(node)
			){
		fprintf(	&_outputFile,
					"/** */\n"
					);
		fprintf(	&_outputFile,
					"namespace %s {\n\n",
					node->_namespaceDesc.getPrefix().getString()
					);
		}

	const char	itcGeneralComment[] = {
		"This class defines the operations that can be performed\n"
		"procedurally by the client.\n"
		};
	printComment(	&_outputFile,	// outputFile
					4,				// tabWidthInCharacters
					60,				// paragraphWidthInCharacters
					0,				// nTabIndention
					itcGeneralComment
					);

	fprintf(	&_outputFile,
				"class Api {\n"
				);
	}

void	ApiHeaderVisit::preVisit(SyncDesc& syncDesc) throw(){
	_syncDesc	= &syncDesc;
	}

void	ApiHeaderVisit::preVisit(CancelDesc& cancelDesc) throw(){
	_cancelDesc	= &cancelDesc;
	}

void	ApiHeaderVisit::preVisit(MessageBodyDesc& messageBodyDesc) throw(){
	}

void	ApiHeaderVisit::preVisit(MessageDesc& messageDesc) throw(){
	clearMessageFields();
	MessageNode*	messageNode	= new MessageNode(messageDesc);
	_messageNodes.put(messageNode);
	}

void	ApiHeaderVisit::preVisit(InputDesc& inputDesc) throw(){
	InputNode*	node	= new InputNode(inputDesc);
	_inputNodes.put(node);
	}

void	ApiHeaderVisit::preVisit(OutputDesc& outputDesc) throw(){
	OutputNode*	node	= new OutputNode(outputDesc);
	_outputNodes.put(node);
	}

void	ApiHeaderVisit::preVisit(InOutDesc& inOutDesc) throw(){
	InOutNode*	node	= new InOutNode(inOutDesc);
	_inOutNodes.put(node);
	}

void	ApiHeaderVisit::preVisit(IncludeDesc& includeDesc) throw(){
	IncludeNode*	node	= new IncludeNode(includeDesc);
	_includeNodes.put(node);
	}


void	ApiHeaderVisit::postVisit(ParseNodeList& parseNodeList) throw(){
	}

void	ApiHeaderVisit::postVisit(NamespaceDesc& namespaceDesc) throw(){
//	popTab();
	}

void	ApiHeaderVisit::postVisit(ItcDesc& itcDesc) throw(){
	// Write the server Api 
	outputApi();

	// Close up the "class Api"
	fprintf(	&_outputFile,
				"\t};\n\n"
				);

	// Close up the namespaces
	for(	NamespaceNode*	node=_namespaceList.first();
			node;
			node	= _namespaceList.next(node)
			){
		fprintf(	&_outputFile,
					"}\n"
					);
		}
	// Close up the header guard
	fprintf(	&_outputFile,
				"#endif\n"
				);

	}

void	ApiHeaderVisit::postVisit(SyncDesc& syncDesc) throw(){
	}

void	ApiHeaderVisit::postVisit(CancelDesc& cancelDesc) throw(){
	}

void	ApiHeaderVisit::postVisit(MessageBodyDesc& messageBodyDesc) throw(){
	}

void	ApiHeaderVisit::postVisit(MessageDesc& messageDesc) throw(){
	if(_syncDesc){
		SyncNode*	syncNode	= new SyncNode(messageDesc,*_syncDesc);
		_syncNodes.put(syncNode);
		syncNode->_inputNodes	= _inputNodes;
		syncNode->_outputNodes	= _outputNodes;
		syncNode->_inOutNodes	= _inOutNodes;
		}
	}

void	ApiHeaderVisit::postVisit(InputDesc& inputDesc) throw(){
	}

void	ApiHeaderVisit::postVisit(OutputDesc& outputDesc) throw(){
	}

void	ApiHeaderVisit::postVisit(InOutDesc& inOutDesc) throw(){
	}

void	ApiHeaderVisit::postVisit(IncludeDesc& includeDesc) throw(){
	}

void	ApiHeaderVisit::appendToHeaderGuard(const char* name) throw(){
	_headerGuard	+= "_";
	_headerGuard	+= name;
	}

void	ApiHeaderVisit::clearMessageFields() throw(){
		InputNode*	inputNode;
		while((inputNode=_inputNodes.get())){
//			delete inputNode;
			}
		OutputNode*	outputNode;
		while((outputNode=_outputNodes.get())){
//			delete outputNode;
			}
		InOutNode*	inOutNode;
		while((inOutNode=_inOutNodes.get())){
			if(!_inOutNodes.first()){
				continue;
				}
//			delete inOutNode;
			}
		_syncDesc	= 0;
		_cancelDesc	= 0;
	}

void	ApiHeaderVisit::outputApi() throw(){
	fprintf(	&_outputFile,
				"\tpublic:\n"
				"\t\t/** Make the compiler happy.\n"
				"\t\t */\n"
				"\t\tvirtual ~Api(){};\n\n"
				);

	for(	SyncNode*	syncNode	= _syncNodes.first();
			syncNode;
			syncNode	= _syncNodes.next(syncNode)
			){
		outputMessageApi(*syncNode);
		}
	}

void	ApiHeaderVisit::outputMessageApi(	SyncNode&			syncNode
										) throw(){
	printComment(	&_outputFile,	// outputFile
					4,				// tabWidthInCharacters
					60,				// paragraphWidthInCharacters
					2,				// nTabIndention
					syncNode._syncDesc.getComment().getString()
					);
	fprintf(	&_outputFile,
				"\t\tvirtual %s\t%s(\t",
				syncNode._syncDesc.getType().getString(),
				syncNode._syncDesc.getName().getString()
				);


	bool	first	= true;
	// Write argument list inputs
	InputNode*	inputNode;

	for(	inputNode=syncNode._inputNodes.first();
			inputNode;
			inputNode=syncNode._inputNodes.next(inputNode)
		){
		if(!first){
			fprintf(&_outputFile,",\n");
			}
		else {
			fprintf(&_outputFile,"\n");
			}
		first	= false;
		fprintf(	&_outputFile,
					"\t\t\t\t\t\t\t%s\t%s",
					inputNode->_inputDesc.getType().getString(),
					inputNode->_inputDesc.getName().getString()
					);
		}

	// Write argument list input/outputs
	InOutNode*	inOutNode;
	for(	inOutNode=syncNode._inOutNodes.first();
			inOutNode;
			inOutNode=syncNode._inOutNodes.next(inOutNode)
		){
		if(!first){
			fprintf(&_outputFile,",\n");
			}
		else {
			fprintf(&_outputFile,"\n");
			}
		first	= false;
		fprintf(	&_outputFile,
					"\t\t\t\t\t\t\t%s\t%s",
					inOutNode->_inOutDesc.getType().getString(),
					inOutNode->_inOutDesc.getName().getString()
					);
		}

	// Close argument list
	fprintf(	&_outputFile,
				"\n"
				"\t\t\t\t\t\t\t) throw()=0;\n\n"
				);
	}

void	ApiHeaderVisit::outputNamespaceSeq() throw(){
	for(	NamespaceNode*	node=_namespaceList.first();
			node;
			node	= _namespaceList.next(node)
			){
		fprintf(	&_outputFile,
					"%s::",
					node->_namespaceDesc.getPrefix().getString()
					);
		}
	}

