#ifndef _oscl_demo_itc_synch_
#define _oscl_demo_itc_synch_

#include "api.h"
#include "reqapi.h"

/** */
namespace Oscl {

/** */
namespace Demo {

/**	This class implements synchronous ITC for the interface.
 */
class Sync : public Api {
	private:
		/** This SAP is used to identify the server.
		 */
		Oscl::Demo::Req::Api::SAP	_sap;

	public:
		/** The constructor requires a reference to the server's
			thread/mailbox (myPapi), and a reference to the server's
			request interface.
		 */
		Sync(	Oscl::Mt::Itc::::PostMessageApi&	myPapi,
				Req::Api&				reqApi
				) throw();

		/** It is frequently the case that this Sync class is declared
			within a server class. Since the server's clients need access
			to the server's SAP, and since the Sync class implementation
			needs the same information, this operation exports the SAP
			to the server/context such that it can be distributed to clients
			that use the server asynchronously. In this way, there is
			only a single copy of the SAP for each service.
		 */
		Req::Api::SAP&	getSAP() throw();

	public:
		/**	
		 */
		void	write(	
						const void*	buffer,
						unsigned int	length
						) throw();

		/**	
		 */
		bool	resolve(	
						unsigned char*	xy,
						unsigned char*	buffer,
						unsigned&	length
						) throw();

	};

}
}
#endif
