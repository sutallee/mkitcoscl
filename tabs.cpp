#include <stdlib.h>
#include <stdio.h>
#include "tabs.h"

using namespace ItcCompiler;

TabsVisit::TabsVisit() throw():
		_outputFile(*stdout)
		{
	}

TabsVisit::TabsVisit(FILE& outputFile) throw():
		_outputFile(outputFile)
		{
	}

void	TabsVisit::printStack() throw(){
	Oscl::Queue<Oscl::Strings::Node>	tmp;
	Oscl::Strings::Node*				node;
	while((node=_stack.pop())){
		tmp.push(node);
		}
	while((node=tmp.pop())){
		fprintf(&_outputFile,"%s",node->getString());
		_stack.push(node);
		}
	}

void	TabsVisit::pushTab() throw(){
	Oscl::Strings::Node*	node
		= new Oscl::Strings::Node("\t");
	if(!node){
		fprintf(stderr,"Out of memory\n");
		exit(1);
		}
	_stack.push(node);
	}

void	TabsVisit::popTab() throw(){
	Oscl::Strings::Node*	node	= _stack.pop();
	if(node){
		delete node;
		}
	else{
		fprintf(stderr,"Logic error on stack\n");
		exit(1);
		}
	}

