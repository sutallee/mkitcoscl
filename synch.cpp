#include <new>
#include <stdlib.h>
#include <stdio.h>
#include "synch.h"
#include "comment.h"

using namespace ItcCompiler;

SyncHeaderVisit::SyncHeaderVisit() throw()
		{
	}

SyncHeaderVisit::SyncHeaderVisit(FILE& outputFile) throw():
		TabsVisit(outputFile)
		{
	}

void	SyncHeaderVisit::preVisit(ParseNodeList& parseNodeList) throw(){
	}

void	SyncHeaderVisit::preVisit(NamespaceDesc& namespaceDesc) throw(){
	NamespaceNode*	node	= new NamespaceNode(namespaceDesc);
	_namespaceList.put(node);

	appendToHeaderGuard(namespaceDesc.getPrefix().getString());
	}

void	SyncHeaderVisit::preVisit(ItcDesc& itcDesc) throw(){
	_headerGuard += "_itc_synch_";
	_headerGuard.convertToLowerCase();

	fprintf(	&_outputFile,
				"#ifndef %s\n"
				"#define %s\n\n",
				_headerGuard.getString(),
				_headerGuard.getString()
				);

	fprintf(	&_outputFile,
				"#include \"api.h\"\n"
				"#include \"reqapi.h\"\n"
				);

	fprintf(	&_outputFile,
			"\n"
			);

	for(	NamespaceNode*	node=_namespaceList.first();
			node;
			node	= _namespaceList.next(node)
			){
		fprintf(	&_outputFile,
					"/** */\n"
					);
		fprintf(	&_outputFile,
					"namespace %s {\n\n",
					node->_namespaceDesc.getPrefix().getString()
					);
		}

	const char	itcGeneralComment[] = {
		"This class implements synchronous ITC for the interface.\n"
		};

	printComment(	&_outputFile,	// outputFile
					4,				// tabWidthInCharacters
					60,				// paragraphWidthInCharacters
					0,				// nTabIndention
					itcGeneralComment
					);

	fprintf(	&_outputFile,
				"class Sync : public Api {\n"
				"\tprivate:\n"
				"\t\t/** This SAP is used to identify the server.\n"
				"\t\t */\n"
				"\t\t"
				);

	outputNamespaceSeq();

	fprintf(	&_outputFile,
				"Req::Api::ConcreteSAP\t_sap;\n\n"
				"\tpublic:\n"
				"\t\t/** The constructor requires a reference to the server's\n"
				"\t\t\tthread/mailbox (myPapi), and a reference to the server's\n"
				"\t\t\trequest interface.\n"
				"\t\t */\n"
				"\t\tSync(\tOscl::Mt::Itc::PostMsgApi&\tmyPapi,\n"
				"\t\t\t\tReq::Api&\t\t\t\treqApi\n"
				"\t\t\t\t) throw();\n\n"
				);

	fprintf(	&_outputFile,
				"\t\t/** It is frequently the case that this Sync class is declared\n"
				"\t\t\twithin a server class. Since the server's clients need access\n"
				"\t\t\tto the server's SAP, and since the Sync class implementation\n"
				"\t\t\tneeds the same information, this operation exports the SAP\n"
				"\t\t\tto the server/context such that it can be distributed to clients\n"
				"\t\t\tthat use the server asynchronously. In this way, there is\n"
				"\t\t\tonly a single copy of the SAP for each service.\n"
				"\t\t */\n"
				"\t\tReq::Api::SAP&\tgetSAP() throw();\n\n"
				);
	}

void	SyncHeaderVisit::preVisit(SyncDesc& syncDesc) throw(){
	_syncDesc	= &syncDesc;
	}

void	SyncHeaderVisit::preVisit(CancelDesc& cancelDesc) throw(){
	_cancelDesc	= &cancelDesc;
	}

void	SyncHeaderVisit::preVisit(MessageBodyDesc& messageBodyDesc) throw(){
	}

void	SyncHeaderVisit::preVisit(MessageDesc& messageDesc) throw(){
	clearMessageFields();
	MessageNode*	messageNode	= new MessageNode(messageDesc);
	_messageNodes.put(messageNode);
	}

void	SyncHeaderVisit::preVisit(InputDesc& inputDesc) throw(){
	InputNode*	node	= new InputNode(inputDesc);
	_inputNodes.put(node);
	}

void	SyncHeaderVisit::preVisit(OutputDesc& outputDesc) throw(){
	OutputNode*	node	= new OutputNode(outputDesc);
	_outputNodes.put(node);
	}

void	SyncHeaderVisit::preVisit(InOutDesc& inOutDesc) throw(){
	InOutNode*	node	= new InOutNode(inOutDesc);
	_inOutNodes.put(node);
	}

void	SyncHeaderVisit::preVisit(IncludeDesc& includeDesc) throw(){
	IncludeNode*	node	= new IncludeNode(includeDesc);
	_includeNodes.put(node);
	}


void	SyncHeaderVisit::postVisit(ParseNodeList& parseNodeList) throw(){
	}

void	SyncHeaderVisit::postVisit(NamespaceDesc& namespaceDesc) throw(){
//	popTab();
	}

void	SyncHeaderVisit::postVisit(ItcDesc& itcDesc) throw(){
	// Write the server Api 
	outputApi();

	// Close up the "class Api"
	fprintf(	&_outputFile,
				"\t};\n\n"
				);

	for(	NamespaceNode*	node=_namespaceList.first();
			node;
			node	= _namespaceList.next(node)
			){
		fprintf(	&_outputFile,
					"}\n"
					);
		}
	// Close up the header guard
	fprintf(	&_outputFile,
				"#endif\n"
				);

	}

void	SyncHeaderVisit::postVisit(SyncDesc& syncDesc) throw(){
	}

void	SyncHeaderVisit::postVisit(CancelDesc& cancelDesc) throw(){
	}

void	SyncHeaderVisit::postVisit(MessageBodyDesc& messageBodyDesc) throw(){
	}

void	SyncHeaderVisit::postVisit(MessageDesc& messageDesc) throw(){
	if(_syncDesc){
		SyncNode*	syncNode	= new SyncNode(messageDesc,*_syncDesc);
		_syncNodes.put(syncNode);
		syncNode->_inputNodes	= _inputNodes;
		syncNode->_outputNodes	= _outputNodes;
		syncNode->_inOutNodes	= _inOutNodes;
		}
	}

void	SyncHeaderVisit::postVisit(InputDesc& inputDesc) throw(){
	}

void	SyncHeaderVisit::postVisit(OutputDesc& outputDesc) throw(){
	}

void	SyncHeaderVisit::postVisit(InOutDesc& inOutDesc) throw(){
	}

void	SyncHeaderVisit::postVisit(IncludeDesc& includeDesc) throw(){
	}

void	SyncHeaderVisit::appendToHeaderGuard(const char* name) throw(){
	_headerGuard	+= "_";
	_headerGuard	+= name;
	}

void	SyncHeaderVisit::clearMessageFields() throw(){
		InputNode*	inputNode;
		while((inputNode=_inputNodes.get())){
//			delete inputNode;
			}
		OutputNode*	outputNode;
		while((outputNode=_outputNodes.get())){
//			delete outputNode;
			}
		InOutNode*	inOutNode;
		while((inOutNode=_inOutNodes.get())){
			if(!_inOutNodes.first()){
				continue;
				}
//			delete inOutNode;
			}
		_syncDesc	= 0;
		_cancelDesc	= 0;
	}

void	SyncHeaderVisit::outputApi() throw(){
	fprintf(	&_outputFile,
				"\tpublic:\n"
				);

	for(	SyncNode*	syncNode	= _syncNodes.first();
			syncNode;
			syncNode	= _syncNodes.next(syncNode)
			){
		outputMessageApi(*syncNode);
		}
	}

void	SyncHeaderVisit::outputMessageApi(	SyncNode&			syncNode
										) throw(){
	printComment(	&_outputFile,	// outputFile
					4,				// tabWidthInCharacters
					60,				// paragraphWidthInCharacters
					2,				// nTabIndention
					syncNode._syncDesc.getComment().getString()
					);
	fprintf(	&_outputFile,
				"\t\t%s\t%s(\t",
				syncNode._syncDesc.getType().getString(),
				syncNode._syncDesc.getName().getString()
				);


	bool	first		= true;
	// Write argument list inputs
	InputNode*	inputNode;

	for(	inputNode=syncNode._inputNodes.first();
			inputNode;
			inputNode=syncNode._inputNodes.next(inputNode)
		){
		if(!first){
			fprintf(&_outputFile,",\n");
			}
		else {
			fprintf(&_outputFile,"\n");
			}
		first	= false;
		fprintf(	&_outputFile,
					"\t\t\t\t\t\t%s\t%s",
					inputNode->_inputDesc.getType().getString(),
					inputNode->_inputDesc.getName().getString()
					);
		}

	// Write argument list input/outputs
	InOutNode*	inOutNode;
	for(	inOutNode=syncNode._inOutNodes.first();
			inOutNode;
			inOutNode=syncNode._inOutNodes.next(inOutNode)
		){
		if(!first){
			fprintf(&_outputFile,",\n");
			}
		else {
			fprintf(&_outputFile,"\n");
			}
		first	= false;
		fprintf(	&_outputFile,
					"\t\t\t\t\t\t%s\t%s",
					inOutNode->_inOutDesc.getType().getString(),
					inOutNode->_inOutDesc.getName().getString()
					);
		}

	// Close argument list
	fprintf(	&_outputFile,
				"\n"
				"\t\t\t\t\t\t) throw();\n\n"
				);
	}

void	SyncHeaderVisit::outputNamespaceSeq() throw(){
	for(	NamespaceNode*	node=_namespaceList.first();
			node;
			node	= _namespaceList.next(node)
			){
		fprintf(	&_outputFile,
					"%s::",
					node->_namespaceDesc.getPrefix().getString()
					);
		}
	}

