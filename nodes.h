#ifndef _nodesh_
#define _nodesh_
#include "tabs.h"
#include "nodes.h"

namespace ItcCompiler {

/** */
class NamespaceNode : public Oscl::QueueItem {
	public:
		const NamespaceDesc&	_namespaceDesc;
		NamespaceNode(const NamespaceDesc& namespaceDesc) throw():
				_namespaceDesc(namespaceDesc)
				{
			}
	};

/** */
class IncludeNode : public Oscl::QueueItem {
	public:
		const IncludeDesc&	_includeDesc;
		IncludeNode(const IncludeDesc& includeDesc) throw():
				_includeDesc(includeDesc)
				{
			}
	};

/** */
class InputNode : public Oscl::QueueItem {
	public:
		const InputDesc&	_inputDesc;
		InputNode(const InputDesc& inputDesc) throw():
				_inputDesc(inputDesc)
				{
			}
	};

/** */
class OutputNode : public Oscl::QueueItem {
	public:
		const OutputDesc&	_outputDesc;
		OutputNode(const OutputDesc& outputDesc) throw():
				_outputDesc(outputDesc)
				{
			}
	};

/** */
class InOutNode : public Oscl::QueueItem {
	public:
		const InOutDesc&	_inOutDesc;
		InOutNode(const InOutDesc& inOutDesc) throw():
				_inOutDesc(inOutDesc)
				{
			}
	};

/** */
class MessageNode : public Oscl::QueueItem {
	public:
		const MessageDesc&	_messageDesc;
		MessageNode(const MessageDesc& messageDesc) throw():
				_messageDesc(messageDesc)
				{
			}
	};

/** */
class CancelNode : public Oscl::QueueItem {
	public:
		const MessageDesc&	_messageDesc;
		CancelNode(const MessageDesc& messageDesc) throw():
				_messageDesc(messageDesc)
				{
			}
	};

/** */
class SyncNode : public Oscl::QueueItem {
	public:
		const MessageDesc&		_messageDesc;
		const SyncDesc&			_syncDesc;

		Oscl::Queue<InputNode>	_inputNodes;
		Oscl::Queue<OutputNode>	_outputNodes;
		Oscl::Queue<InOutNode>	_inOutNodes;

		SyncNode(	const MessageDesc&	messageDesc,
					const SyncDesc&		syncDesc
					) throw():
				_messageDesc(messageDesc),
				_syncDesc(syncDesc)
				{
			}
	};

}

#endif
